close all
clear all
clc


%% IFAM
% author: Kiran Manjunatha
% created:     17.01.23
% last edited: 18.01.23

addpath github_repo
[node, element_artfree, element_artstented, element_media,...
    element_adventitia,...
    nodeset_ss, size_ss,...
    nodeset_aa, size_aa,...
    nodeset_z1, size_z1,...
    nodeset_z2, size_z2,...
    nodeset_hec, size_hec] =...
    readinp_no_stent('ivo_240320.inp');

nodeset_z = [nodeset_z1; nodeset_z1];
size_z = size_z1 + size_z2;

nelems = size(element_adventitia,1) +...
         size(element_artfree,1) + size(element_artstented,1) + ...
         size(element_media,1);

nnodes = size(node,1);

fprintf('---------------------------------------MESH INFORMATION----------------------------------------\n');

fprintf('Total number of nodes    = %d\n',nnodes);
fprintf('Total number of elements = %d\n',nelems);

fprintf('------------------------------------------------------------------------------------------------\n\n\n\n');
fprintf('------------------------------------------------------------------------------------------------\n');
fprintf('Media elements                                               = %d\n',size(element_media,1));
fprintf('Adventitia elements                                          = %d\n',size(element_adventitia,1));
fprintf('Free elements on the inner surface of media                  = %d\n',size(element_artfree,1));
fprintf('Elements in contact with stent on the inner surface of media = %d\n',size(element_artstented,1));
fprintf('------------------------------------------------------------------------------------------------\n');

mkdir FEAP
currentFolder = pwd;

%% nodes generation
fileID = fopen(fullfile(currentFolder,'FEAP/nodes'),'w');
for i = 1:size(node,1)
    fprintf(fileID,'%d, ,  %f,  %f, %f\n',...
        node(i,1),...
        node(i,2), node(i,3), node(i,4));
end
fclose(fileID); 

%% element generation

fileID = fopen(fullfile(currentFolder,'FEAP/media_elems'),'w');
for i = 1:size(element_media,1)
    fprintf(fileID,'%d, 1,  1,  %d,  %d, %d, %d, %d, %d, %d, %d \n',...
        element_media(i,1),...
        element_media(i,2), element_media(i,3), element_media(i,4),...
        element_media(i,5), element_media(i,6), element_media(i,7),...
        element_media(i,8), element_media(i,9));
end
fclose(fileID); 

fileID = fopen(fullfile(currentFolder,'FEAP/adv_elems'),'w');
for i = 1:size(element_adventitia,1)
    fprintf(fileID,'%d, 1,  2,  %d,  %d, %d, %d, %d, %d, %d, %d \n',...
        element_adventitia(i,1),...
        element_adventitia(i,2), element_adventitia(i,3), element_adventitia(i,4),...
        element_adventitia(i,5), element_adventitia(i,6), element_adventitia(i,7),...
        element_adventitia(i,8), element_adventitia(i,9));
end
fclose(fileID); 

% fileID = fopen(fullfile(currentFolder,'FEAP/stent_elems'),'w');
% for i = 1:size(element_stent,1)
%     fprintf(fileID,'%d, 1,  5,  %d,  %d, %d, %d, %d, %d, %d, %d \n',...
%         element_stent(i,1),...
%         element_stent(i,2), element_stent(i,3), element_stent(i,4),...
%         element_stent(i,5), element_stent(i,6), element_stent(i,7),...
%         element_stent(i,8), element_stent(i,9));
% end
% fclose(fileID); 


fileID = fopen(fullfile(currentFolder,'FEAP/af_elems'),'w');
for i = 1:size(element_artfree,1)
    fprintf(fileID,'%d, 1,  3,  %d,  %d, %d, %d\n',...
        element_artfree(i,1),...
        element_artfree(i,2), element_artfree(i,3),...
        element_artfree(i,4), element_artfree(i,5));
end
fclose(fileID); 

fileID = fopen(fullfile(currentFolder,'FEAP/as_elems'),'w');
for i = 1:size(element_artstented,1)
    fprintf(fileID,'%d, 1,  4,  %d,  %d, %d, %d\n',...
        element_artstented(i,1),...
        element_artstented(i,2), element_artstented(i,3),...
        element_artstented(i,4), element_artstented(i,5));
end
fclose(fileID);

%% nodal boundary conditions

% ----------------------------triad calculations--------------------------!
c1 = [7.638 0.4201 -0.2052]; % center of Z1 face
x11 = [7.709 0.4069 0.9673]; % basis on Z1 face
x12 = [7.476 1.582 0.1804];  % basis on Z1 face

c2 = [1.189 -0.02068 0.009398]; % center of Z2 face
x21 = [1.192 0.976 0.8432];     % basis on Z2 face
x22 = [1.183 -0.6889 1.124];    % basis on Z2 face

X1 = x11 - c1;
X2 = x12 - c1;

X1 = X1./norm(X1);
X2 = X2./norm(X2);

X3 = cross(X1, X2);
X3 = X3./norm(X3);

X2 = cross(X1,X3);
X2 = X2./norm(X2);

Y1 = x21 - c2;
Y2 = x22 - c2;

Y1 = Y1./norm(Y1);
Y2 = Y2./norm(Y2);

Y3 = cross(Y1,Y2);
Y3 = Y3./norm(Y3);

Y2 = cross(Y1,Y3);
Y2 = Y2./norm(Y2);

% ----------------------------triad calculations--------------------------!

fileID = fopen(fullfile(currentFolder,'FEAP/aa_XYZ'),'w');
for i = 1:length(nodeset_aa)
    fprintf(fileID,'%d, 0,  0,  0,  1\n',nodeset_aa(i));
end
fclose(fileID);   

fileID = fopen(fullfile(currentFolder,'FEAP/ss_XYZ'),'w');
for i = 1:length(nodeset_ss)
    fprintf(fileID,'%d, 0,  1,  1,  1\n',nodeset_ss(i));
end
fclose(fileID);

fileID = fopen(fullfile(currentFolder,'FEAP/art_Z1'),'w');
for i = 1:length(nodeset_z1)
    fprintf(fileID,'%d, 0,  0,  0,  1\n',nodeset_z1(i));
end
fclose(fileID);

fileID = fopen(fullfile(currentFolder,'FEAP/art_Z2'),'w');
for i = 1:length(nodeset_z2)
    fprintf(fileID,'%d, 0,  0,  0,  1\n',nodeset_z2(i));
end
fclose(fileID);

triad_long = importdata('../fiber_alignment_outer_adventitia/triad_long.txt');
triad_circ = importdata('../fiber_alignment_outer_adventitia/triad_circ.txt');
triad_rad = importdata('../fiber_alignment_outer_adventitia/triad_rad.txt');

fileID = fopen(fullfile(currentFolder,'FEAP/triadZ'),'w');
for i = 1:length(nodeset_z1)
    fprintf(fileID,'%d, 0, %f,  %f,  %f,  %f,  %f,  %f,  %f,  %f,  %f \n',...
        nodeset_z1(i),X1(1),X1(2),X1(3),X2(1),X2(2),X2(3),X3(1),X3(2),X3(3));
end
for i = 1:length(nodeset_z2)
    fprintf(fileID,'%d, 0, %f,  %f,  %f,  %f,  %f,  %f,  %f,  %f,  %f \n',...
        nodeset_z2(i),Y1(1),Y1(2),Y1(3),Y2(1),Y2(2),Y2(3),Y3(1),Y3(2),Y3(3));
end
for i = 1:length(nodeset_aa)
    fprintf(fileID,'%d, 0, %f,  %f,  %f,  %f,  %f,  %f,  %f,  %f,  %f \n',...
        nodeset_aa(i),triad_long(i,1),triad_long(i,2),triad_long(i,3),...
        triad_circ(i,1),triad_circ(i,2),triad_circ(i,3),...
        triad_rad(i,1),triad_rad(i,2),triad_rad(i,3));
end
fclose(fileID);

% fileID = fopen(fullfile(currentFolder,'FEAP/triadZ2'),'w');
% for i = 1:length(nodeset_z2)
%     fprintf(fileID,'%d, 0, %f,  %f,  %f,  %f,  %f,  %f,  %f,  %f,  %f \n',...
%         nodeset_z2(i),Y1(1),Y1(2),Y1(3),Y2(1),Y2(2),Y2(3),Y3(1),Y3(2),Y3(3));
% end
% fclose(fileID);

%% nodal initial conditions
fileID = fopen(fullfile(currentFolder,'FEAP/hec_init'),'w');
for i = 1:length(nodeset_hec)
    fprintf(fileID,'%d, , 0.d00, 0.d00, 0.d00, 0.d00, 0.d00, 1.d00, 0.d00, 1.d00, 1.d00, 0.d00, 7.d00, 0.d00\n',nodeset_hec(i));
end
fclose(fileID);

%% interface nodal data for Anna
boundary_node_numbers = unique([unique(element_artstented(:,2)).',...
    unique(element_artstented(:,3)).',unique(element_artstented(:,4)).',...
    unique(element_artstented(:,5)).',unique(element_artfree(:,2)).',...
    unique(element_artfree(:,3)).',unique(element_artfree(:,4)).',...
    unique(element_artfree(:,5)).']);

fileID = fopen(fullfile(currentFolder,'FEAP/fbnodaldata'),'w');
for i = 1:length(boundary_node_numbers)
    fprintf(fileID,'%d, %f, %f, %f\n',boundary_node_numbers(i), node(boundary_node_numbers(i),2),node(boundary_node_numbers(i),3),node(boundary_node_numbers(i),4));
end
fclose(fileID);
