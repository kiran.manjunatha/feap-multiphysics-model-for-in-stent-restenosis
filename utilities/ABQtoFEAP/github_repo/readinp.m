function [node, element_artfree,element_artstented,...
    element_media, element_adventitia,...
    ns_array_ss,size_ss,...
    ns_array_aa,size_aa,...
    ns_array_z,size_z,...
    ns_array_hec,size_hec] = readinp(filename)
% read abaqus inp file to get nodes, elements and element type of a mesh
% only support one element type 
%% Code Information
% author      :  Ji Wan
% affiliation :  Wuhan University
% date        :  2022 / 02 / 13
% e-mail      :  wanji at whu.edu.cn
%% input arguments:
% filename: the full path with your inp file name, 
% e.g., filename = 'C:/test/exam1.inp'
%% output arguments:
% node : nodes of the model
% element : elements of the model
% eType : the element type
%% Requirement:
%% Matlab 2016b or later (due to the use of 'split' function)
%% Examples(see also in demo)
% filename = 'exam1.inp';
% [node, element, eType] = readinp(filename);
s   = fileread(filename);
s   = lower(s);
q   = split(s, '*');
% read nodes
str = 'node';
np  = arrayfun(@(i)strncmp(q{i},'node',length(str)), 1:numel(q), 'uniform', 1);
ns  = q{np};
nsp = strfind(ns, newline);
ns  = ns(nsp(1)+1:nsp(end)-1);
node = str2num(ns);


% read elements - artery free
str = 'element,type=s4,elset=artery_free';
ep  = arrayfun(@(i)strncmp(q{i},'element,type=s4,elset=artery_free',...
    length(str)), 1:numel(q), 'uniform', 1);
es_artfree  = q{ep};
esp = strfind(es_artfree, newline);

% elements
es_artfree  = es_artfree(esp(1)+1:esp(end)-1);
es_artfree  = strrep(es_artfree, char([44,13,10]), char(44));
element_artfree = str2num(es_artfree);

% read elements - artery stented
str = 'element,type=s4,elset=artery_stented';
ep  = arrayfun(@(i)strncmp(q{i},'element,type=s4,elset=artery_stented',...
    length(str)), 1:numel(q), 'uniform', 1);
es_artstented  = q{ep};
esp = strfind(es_artstented, newline);

% elements
es_artstented  = es_artstented(esp(1)+1:esp(end)-1);
es_artstented  = strrep(es_artstented, char([44,13,10]), char(44));
element_artstented = str2num(es_artstented);

% % read elements - adventitia outer
% ep  = arrayfun(@(i)strncmp(q{i},'element,type=s4r,elset=adventitia_outer',39), 1:numel(q), 'uniform', 1);
% es_advouter  = q{ep};
% esp = strfind(es_advouter, newline);
% 
% % elements
% es_advouter  = es_advouter(esp(1)+1:esp(end)-1);
% es_advouter  = strrep(es_advouter, char([44,13,10]), char(44));
% element_advouter = str2num(es_advouter);

% read elements - media solid
str = 'element,type=c3d8,elset=solidmap_media';
ep  = arrayfun(@(i)strncmp(q{i},'element,type=c3d8,elset=solidmap_media',...
    length(str)), 1:numel(q), 'uniform', 1);
es_media  = q{ep};
esp = strfind(es_media, newline);

% elements
es_media  = es_media(esp(1)+1:esp(end)-1);
es_media  = strrep(es_media, char([44,13,10]), char(44));
element_media = str2num(es_media);


% read elements - adventitia solid
str = 'element,type=c3d8,elset=solidmap_adventitia';
ep  = arrayfun(@(i)strncmp(q{i},'element,type=c3d8,elset=solidmap_adventitia',...
    length(str)), 1:numel(q), 'UniformOutput', 1);
es_adventitia  = q{ep};
esp = strfind(es_adventitia, newline);

% elements
es_adventitia  = es_adventitia(esp(1)+1:esp(end)-1);
es_adventitia  = strrep(es_adventitia, char([44,13,10]), char(44));
element_adventitia = str2num(es_adventitia);

% % read elements - stent
% str = 'element,type=c3d8,elset=stent';
% ep  = arrayfun(@(i)strncmp(q{i},'element,type=c3d8,elset=stent',...
%     length(str)), 1:numel(q), 'UniformOutput', 1);
% es_stent  = q{ep};
% esp = strfind(es_stent, newline);

% elements
% es_stent  = es_stent(esp(1)+1:esp(end)-1);
% es_stent  = strrep(es_stent, char([44,13,10]), char(44));
% element_stent = str2num(es_stent);

% read nodesets - stented surface
str = 'nset, nset=stent_surfs';
ep  = arrayfun(@(i)strncmp(q{i},str,length(str)), 1:numel(q), 'UniformOutput', 1);
ns_ss  = q{ep};
ns_ss = ns_ss(length(str)+1:end);
ns_ss = strrep(ns_ss,',','');
[ns_array_ss,size_ss] = sscanf(ns_ss,'%d');

% read nodesets - adventitia outer surface
str = 'nset, nset=adventitia_outer';
ep  = arrayfun(@(i)strncmp(q{i},str,length(str)), 1:numel(q), 'UniformOutput', 1);
ns_aa  = q{ep};
ns_aa = ns_aa(length(str)+1:end);
ns_aa = strrep(ns_aa,',','');
[ns_array_aa,size_aa] = sscanf(ns_aa,'%d');

% read nodesets - end bc
str = 'nset, nset=bc_z1';
ep  = arrayfun(@(i)strncmp(q{i},str,length(str)), 1:numel(q), 'UniformOutput', 1);
ns_z1  = q{ep};
ns_z1 = ns_z1(length(str)+1:end);
ns_z1 = strrep(ns_z1,',','');
[ns_array_z1,size_z1] = sscanf(ns_z1,'%d');

str = 'nset, nset=bc_z2';
ep  = arrayfun(@(i)strncmp(q{i},str,length(str)), 1:numel(q), 'UniformOutput', 1);
ns_z2  = q{ep};
ns_z2 = ns_z2(length(str)+1:end);
ns_z2 = strrep(ns_z2,',','');
[ns_array_z2,size_z2] = sscanf(ns_z2,'%d');

ns_array_z = [ns_array_z1; ns_array_z2];
size_z = size_z1 + size_z2;

% read nodesets - initial healthy EC surface
str = 'nset, nset=initial_healthy_ec';
ep  = arrayfun(@(i)strncmp(q{i},str,length(str)), 1:numel(q), 'UniformOutput', 1);
ns_hec  = q{ep};
ns_hec = ns_hec(length(str)+1:end);
ns_hec = strrep(ns_hec,',','');
[ns_array_hec,size_hec] = sscanf(ns_hec,'%d');

end