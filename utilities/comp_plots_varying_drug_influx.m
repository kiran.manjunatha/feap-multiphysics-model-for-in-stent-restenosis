clear all
close all
clc

q0 = readmatrix("../iter23_red_cPprolif_slope_etaPratio_0_2/q0/data_q0_disp.csv");
q0_5 = readmatrix("q0_5/data_q0_5_disp.csv");
q2 = readmatrix("q2/data_q2_disp.csv");
q10 = readmatrix("q10/data_q10_disp.csv");
q50 = readmatrix("q50/data_q50_disp.csv");


figure(1)
x = q0(:,3) + q0(:,33);
plot(x,-1.0*q0(:,2),'LineWidth',1.0,'Color','red')
hold on

figure(1)
x = q0_5(:,3) + q0_5(:,33);
plot(x,-1.0*q0_5(:,2),'LineWidth',1.0,'Color','#EDB120') %yellow
hold on

figure(1)
x = q2(:,3) + q2(:,33);
plot(x,-1.0*q10(:,2),'LineWidth',1.0,'Color','#77AC30')
hold on

figure(1)
x = q10(:,3) + q10(:,33);
plot(x,-1.0*q10(:,2),'LineWidth',1.0,'Color','magenta')
hold on

figure(1)
x = q50(:,3) + q50(:,33);
plot(x,-1.0*q50(:,2),'LineWidth',1.0,'Color','blue')
hold on

xlim([-4 12])
ylim([0. 1.2])
pbaspect([8 1 1])
ax = gca;
ax.FontSize = 14;
ax.TickLabelInterpreter = 'latex';
xlabel('Z [mm]','Interpreter','latex','FontSize',14)
ylabel('Neointima thickness [mm]','FontSize',20)

legend('$\bar{q}^{\textrm{ref}}_{{}_D} = 0.0 $ [nM-mm/day]',...
    '$\bar{q}^{\textrm{ref}}_{{}_D} = 0.5 $ [nM-mm/day]',...
    '$\bar{q}^{\textrm{ref}}_{{}_D} = 2.0 $ [nM-mm/day]',...
    '$\bar{q}^{\textrm{ref}}_{{}_D} = 10.0 $ [nM-mm/day]',...
    '$\bar{q}^{\textrm{ref}}_{{}_D} = 50.0 $ [nM-mm/day]',...
    'Interpreter','latex','FontSize',14,'location','northoutside')

saveas(gcf,'neointima_365[days]','epsc')
grid on
grid minor
