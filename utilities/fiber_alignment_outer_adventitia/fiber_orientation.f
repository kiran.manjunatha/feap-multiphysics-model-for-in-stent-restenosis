        module helpersubroutines

        contains

        subroutine read_in_text_file(file_name,read_in_array,rows,cols)

            character(128), intent(in) :: file_name
            character(128):: buffer

            integer :: strlen, rows, cols, i, io
            double precision, dimension(:,:),
     &                        allocatable :: read_in_array


            open (1, file = file_name, status='old', action='read')

            !Count the number of columns

            read(1,'(a)') buffer !read first line WITH SPACES INCLUDED
            rewind(1) !Get back to the file beginning

            strlen = len(buffer) !Find the REAL length of a string read
            do while (buffer(strlen:strlen) == ' ') 
                strlen = strlen - 1 
            enddo

            cols=0 !Count the number of spaces in the first line
            do i=0,strlen
                if (buffer(i:i) == ',') then
                    cols=cols+1
                endif
            enddo

            cols = cols + 1

            !Count the number of rows

            rows = 0 !Count the number of lines in a file
            do
                read(1,*,iostat=io)
                if (io/=0) exit
                rows = rows + 1
            end do

            rewind(1)

            !write(*,*) 'rows: ', rows
            !write(*,*) 'cols: ', cols

            allocate(read_in_array(rows,cols))

            do i=1,rows,1
                read(1,*) read_in_array(i,:)
            enddo

            close (1)

        end subroutine read_in_text_file


        subroutine projection_onto_CL(node_X,
     &                                center_line,n_cp,n_dim,
     &                                proj,long_vec,rad_vec)

            double precision,intent(in),
     &      dimension(n_cp,n_dim):: center_line
            double precision,dimension(3),intent(in) :: node_X
            double precision,dimension(3) :: proj_vec
            double precision,dimension(3),intent(out) :: proj
            
            integer :: i

            double precision, dimension(3) :: line_vec,rad_vec,long_vec

            double precision :: dist, min_dist

            double precision,dimension(3) :: proj_temp,rad_vec_temp


            !   A---D-------------B
            !    .  .
            !     . .
            !       C

            !AD = AB * (AB dot AC) / (AB dot AB)
            !D = (dx, dy) = A + AD
            
            min_dist = 100.d00
            do i = 1,n_cp-1
                line_vec = center_line(i+1,:) - center_line(i,:)

                if ((dot_product(line_vec,
     &             (node_X-center_line(i,:))).gt.1.d-16).and.
     &             (dot_product(-1.d00*line_vec,
     &             (node_X-center_line(i+1,:)))).gt.1.d-16) then
                    proj_vec = (dot_product(line_vec,
     &                         (node_X-center_line(i,:)))/
     &                         dot_product(line_vec,line_vec))*line_vec
                    proj_temp = center_line(i,:) + proj_vec

                    rad_vec_temp = node_X - proj_temp
                    dist = norm2(rad_vec_temp)

                else if(abs(dot_product(line_vec,
     &             (node_X-center_line(i,:)))).le.0.1) then
                    rad_vec_temp = node_X-center_line(i,:)
                    dist = norm2(rad_vec_temp)
                    proj_temp = center_line(i,:)
                else if(abs(dot_product(-1.d00*line_vec,
     &             (node_X-center_line(i+1,:)))).le.0.1) then
                    rad_vec_temp = node_X-center_line(i+1,:)
                    dist = norm2(rad_vec_temp)
                    proj_temp = center_line(i+1,:)
                end if

                if ((dist.gt.0.d00).and.(dist.lt.min_dist)) then
                    min_dist = dist
                    proj = proj_temp
                    rad_vec = rad_vec_temp/norm2(rad_vec_temp)
                    long_vec = line_vec/norm2(line_vec)
                end if

                !write(*,*) dist
                
            end do

            write(*,*) min_dist

!            if (min_dist.eq.100.d00) then
!                write(*,*) node_X
!                write(*,*) proj
!                write(*,*) rad_vec
!                write(*,*) long_vec
!                write(*,*) dot_product(line_vec,
!     &             (node_X-center_line(i,:)))
!                write(*,*) dot_product(-1.d00*line_vec,
!     &             (node_X-center_line(i+1,:)))
!            end if
            !write(*,*) proj
            !write(*,*) rad_vec
            !write(*,*) long_vec
            !write(*,*) node_X
            


        end subroutine projection_onto_CL


        function cross(a, b)
            double precision, dimension(3) :: cross
            double precision, dimension(3), intent(in) :: a, b
          
            cross(1) = a(2) * b(3) - a(3) * b(2)
            cross(2) = a(3) * b(1) - a(1) * b(3)
            cross(3) = a(1) * b(2) - a(2) * b(1)
        end function cross


      end module helpersubroutines
      
      
      
      
      
      
      program calc_fiber_orientations

      use helpersubroutines

      implicit none
        
        integer :: n_cp,n_node,n_dim,i,n_OA,n_dim_OA
        character(128) :: file_name_CL,file_name_nodes,file_name_OA
        double precision, dimension(:,:), allocatable :: center_line
        double precision, dimension(:,:), allocatable :: nodes
        double precision, dimension(:,:), allocatable ::
     &    outer_adventitia_nodes

        double precision, dimension(3) :: proj, parent_cp1,parent_cp2
        double precision, dimension(3) :: nodes_X

        double precision, dimension(3) :: radial_vec
        double precision, dimension(3) :: longitudinal_vec
        double precision, dimension(3) :: circumferential_vec

        double precision :: gamma

        double precision :: rot_mat1(3,3), rot_mat2(3,3), w_mat(3,3)
        double precision :: identity3(3,3)


        double precision, dimension(3) :: e1, e2

        file_name_CL = 'IvoCenterlinesTXT.txt'
        file_name_nodes = 'Ivo_artery_nodes.txt'
        file_name_OA = 'outer_adventitia_nodes.txt'

        call read_in_text_file(file_name_CL,center_line,n_cp,n_dim)

        !print*, 'Number of center points:', n_cp

        call read_in_text_file(file_name_nodes,nodes,n_node,n_dim)

        call read_in_text_file(file_name_OA,outer_adventitia_nodes,
     &                         n_OA,n_dim_OA)

        !print*, 'Number of nodes:', n_node
        open (unit=111,file="nodes_X.txt",action="write")
        open (unit=222,file="e1.txt",action="write")
        open (unit=333,file="e2.txt",action="write")
        open (unit=444,file="triad_long.txt",action="write")
        open (unit=555,file="triad_rad.txt",action="write")
        open (unit=666,file="triad_circ.txt",action="write")

        do i = 1,n_OA
            nodes_X = nodes(
     &                 int(outer_adventitia_nodes(i,n_dim_OA)),:)
            call projection_onto_CL(nodes_X,
     &                              center_line,n_cp,n_dim,       
     &                              proj,
     &                              longitudinal_vec,
     &                              radial_vec)

            circumferential_vec = cross(longitudinal_vec,
     &                                  radial_vec)

            circumferential_vec = circumferential_vec/
     &                            norm2(circumferential_vec)

            ! Rodrigues formula

            gamma = 45.d00
            
            w_mat = 0.d00
            
            w_mat(1,2) = -1.d00*radial_vec(3)
            w_mat(1,3) = radial_vec(2)
            w_mat(2,1) = radial_vec(3)
            w_mat(2,3) = -1.d00*radial_vec(1)
            w_mat(3,1) = -1.d00*radial_vec(2)
            w_mat(3,2) = radial_vec(1)
            
            identity3 = 0.d00
            identity3(1,1) = 1.d00
            identity3(2,2) = 1.d00
            identity3(3,3) = 1.d00
            
            rot_mat1 = identity3 + SIND(gamma)*w_mat +
     &      (2*(SIND(0.5d00*gamma))**2)*MATMUL(w_mat,w_mat)

            rot_mat2 = identity3 + SIND(-1.d00*gamma)*w_mat +
     &      (2*(SIND(-0.5d00*gamma))**2)*MATMUL(w_mat,w_mat)    

            e1 = MATMUL(rot_mat1,circumferential_vec)

            e2 = MATMUL(rot_mat2,circumferential_vec)

            e1 = (1/norm2(e1))*e1
            e2 = (1/norm2(e2))*e2


            write (111,*) nodes_X(1),', ',
     &                    nodes_X(2),', ',
     &                    nodes_X(3)

            write (222,*)  e1(1),', ', e1(2),', ', e1(3) 
            write (333,*)  e2(1),', ', e2(2),', ', e2(3)
            
            write (444,*)  longitudinal_vec(1),', ',
     &                     longitudinal_vec(2),', ',
     &                     longitudinal_vec(3)
            write (555,*)  radial_vec(1),', ',
     &                     radial_vec(2),', ',
     &                     radial_vec(3)
            write (666,*)  circumferential_vec(1),', ',
     &                     circumferential_vec(2),', ',
     &                     circumferential_vec(3)


        end do

        close(222)
        close(333)
        close(444)
        close(555)
        close(666)


      end program calc_fiber_orientations
