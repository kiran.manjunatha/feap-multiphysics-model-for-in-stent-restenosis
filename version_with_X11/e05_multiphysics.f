      subroutine elmt05(
     &     d,                   ! element data parameters
     &     ul,                  ! element nodal solution parameters
     &     xl,                  ! element noda reference coordinates
     &     ix,                  ! element global node numbers
     &     tl,                  ! element nodal temperature values
     &     s,                   ! element matrix (e.g. stiffness, mass)
     &     r,                   ! element vector (e.g. residual, mass)
     &     ndf,                 ! number of unknowns (max per node)
     &     ndm,                 ! space dimension of mesh
     &     nst,                 ! size of element arrays s and r
     &     isw)                 ! task parameter to control computation
               

!      * * F E A P * * A Finite Element Analysis Program

!....  Copyright (c) 1984-2020: Regents of the University of California
!                               All rights reserved

!-----[--.----+----.----+----.-----------------------------------------]
!     Modification log                                Date (dd/mm/year)
!       Original version                                    01/11/2006
!       Multiphysics bulk element                           17/10/2023
!-----[--.----+----.----+----.-----------------------------------------]
!     Author:  Kiran Manjunatha (kiran.manjunatha@ifam.rwth-aachen.de)

!     Purpose: A multiphysics bulk element for in-stent restenosis
!              Element with all DOFs except EC activated      

!-----[--.----+----.----+----.-----------------------------------------]     

      use omp_lib               ! open MP parallelization
      
      use fiber_alignment       ! Collagen orientation assignment

      implicit none

      include 'bdata.h'
      include 'cdata.h'
      include 'eldata.h'
      include 'elplot.h'
      include 'elcapt.h'
      include 'hdata.h'
      include 'hdatam.h'
      include 'iofile.h'
      include 'rdata.h'
      include 'prstrs.h'
      include 'pdata6.h' 
      include 'strnum.h'
      include 'tdata.h'
      include 'eltran.h'
      include 'augdat.h'
      include 'comblk.h'
      include 'prlod.h'

      double precision :: d(*)            ! Material set parameters
      double precision :: ul(ndf,nen,6)   ! Solution parameters
      double precision :: xl(ndm,nen)     ! Element nodal coordinates
      integer :: ix(*)                    ! Element global node numbers      
      double precision :: tl(*)           ! Element vector (e.g., nodal temperatures)
      integer :: ndf                      ! Maximum no dof's/node
      integer :: ndm                      ! Mesh spatial dimension
      integer :: nst                      ! Element matrix/vector dimension
      integer :: isw                      ! Switch parameter (set by input commands)

      double precision :: s(nst,nst)      ! Element matrix (stiffness, mass, etc.)
      double precision :: r(nst)          ! Element vector (residual, lump mass, etc.)
      double precision :: p(nen)
      
      logical :: pinput, tinput, errchk

!==================================================Parameters==============================================================!
! Growth factors (GF)------------------------------------------------------------------------------------------------------!
! Platelet-derived growth factor (PDGF)------------------------------------------------------------------------------------!
      double precision :: d_P          ! PDGF diffusivity
      double precision :: r_eta        ! PDGF secretion ratio
      double precision :: eta_P        ! PDGF autocrine secretion coefficient
      double precision :: l_P1         ! drug-dose-dependent anti-inflammatory parameter
      double precision :: epsilon_P    ! PDGF receptor internalization coefficient
      double precision :: l_P2         ! steepness coefficient for the drop in PDGF receptors due to high TGF-beta presence
      double precision :: c_T_th       ! TGF-beta threshold for the the drop in PDGF receptors
      double precision :: c_P_healthy  ! PDGF level in healthy blood serum in [mol/mm^3]
      double precision :: k_P          ! scaling for non-dimensionalition (=c_P_healthy)

! Transforming growth factor (TGF)-----------------------------------------------------------------------------------------!
      double precision :: d_T          ! TGF-beta diffusivity
      double precision :: epsilon_T    ! TGF-beta receptor internalization coefficient
      double precision :: c_T_healthy  ! TGF-beta level in healthy blood serum in [mol/mm^3]
      double precision :: k_T          ! scaling for non-dimensionalition (=c_T_healthy)             

! Extracellular matrix (ECM)-----------------------------------------------------------------------------------------------!
      double precision :: eta_C        ! collagen secretion coefficient
      double precision :: epsilon_C    ! collagen degradation coefficient
      double precision :: c_C_threshold! collagen density threshold 
      double precision :: c_C_healthy  ! collagen density in a healthy vessel
      double precision :: k_C          ! scaling for non-dimensionalition (=c_C_healthy) 

! Drug (rapamycin)---------------------------------------------------------------------------------------------------------!
      double precision :: d_D          ! drug diffusivity
      double precision :: epsilon_D1   ! SMC FKBP12 receptor internalization coefficient
      double precision :: c_D_oneNM    ! one [NM] concentration of drug in [mol/mm^3]
      double precision :: k_D          ! scaling for non-dimensionalition (=c_D_oneNM) 

! Smooth muscle cells (SMC)------------------------------------------------------------------------------------------------!
      double precision :: d_S          ! SMC diffusivity
      double precision :: chi_S1       ! SMC chemotactic sensitivity
      double precision :: chi_S2       ! SMC haptotactic sensitivity
      double precision :: l_S1         ! steepness coefficient for activation of haptotaxis
      double precision :: c_P_th       ! PDGF threshold for haptotaxis activation
      double precision :: eta_S        ! SMC proliferation coefficient
      double precision :: l_S2         ! PDGF receptor saturation coefficient
      double precision :: l_S3         ! steepness coefficient for TGF-beta-induced proliferation modulation
      double precision :: A_S          ! maximum efficacy of the drug against SMC proliferation [%]
      double precision :: B_S          ! drug concentration for half efficacy of the drug
      double precision :: alpha        ! Hill coefficient for drug-induced SMC proliferation inhibition
      double precision :: rho_S_healthy! SMC density in a healthy vessel
      double precision :: k_S          ! scaling for non-dimensionalition (=rho_S_healthy) 

! Structure ---------------------------------------------------------------------------------------------------------------!
      double precision :: mu           ! shear modulus for the isotropic part 
      double precision :: lambda       ! Lame parameter pertaining to the bulk modulus of the isotropic part
      double precision :: k1_bar       ! stress-like parameter for collagen fibres
      double precision :: k2           ! exponential coefficient for collagen fibres
      double precision :: kappa        ! dispersion coefficient for collagen orientations
      double precision :: gamma        ! collagen orientation w.r.t circumferential direction

! Simulation parameters ---------------------------------------------------------------------------------------------------!
      integer :: flg_actv                    ! element activation flag
      integer :: nqppd                       ! number of quadrature-points per direction
      integer :: flg_cpl                     ! coupling selection flag
      double precision :: theta_numerical    ! time integration parameter
      integer :: flg_g                       ! growth flag
      integer :: flg_stab                    ! SUPG stabilization flag
      integer :: np_levels                   ! levels of nested parallelism enabled

!==========================================================================================================================!

      integer :: nqp
      integer :: qp
      integer :: i, j
      integer :: lint
      integer :: nnl
      double precision :: quadr(4,8) 
      double precision :: xu(ndm,nen)
      double precision :: xi
      double precision :: eta
      double precision :: zeta
      double precision :: weight
      double precision :: sf(8)
      double precision :: defgrad(3,3)
      double precision :: det_defgrad
      double precision :: c_p_prev_e(8), c_p_iter_e(8) 
      double precision :: c_t_prev_e(8), c_t_iter_e(8)
      double precision :: c_e_prev_e(8), c_e_iter_e(8)
      double precision :: c_d_iter_e(8), c_d_prev_e(8)
      double precision :: rho_E_iter_e(8), rho_E_prev_e(8)
      double precision :: rho_s_prev_e(8), rho_s_iter_e(8)
      double precision :: e1(3),e2(3)
      double precision :: A01(3,3), A02(3,3)
      double precision :: theta
      double precision :: FPK(3,3),FPK_v(9)
      double precision :: cauchy(3,3),cauchy_v(6)
      double precision :: rho_E_healthy, k_E

      double precision :: res_u_qp(8,24)
      double precision :: res_p_qp(8,8)
      double precision :: res_t_qp(8,8)
      double precision :: res_e_qp(8,8)
      double precision :: res_d_qp(8,8)
      double precision :: res_s_qp(8,8)

      double precision :: K_uu_qp(8,24,24)
      double precision :: K_us_qp(8,24,8)
      double precision :: K_ue_qp(8,24,8)
      double precision :: K_pu_qp(8,8,24)
      double precision :: K_tu_qp(8,8,24)
      double precision :: K_eu_qp(8,8,24)
      double precision :: K_su_qp(8,8,24)
      double precision :: K_pp_qp(8,8,8)
      double precision :: K_tt_qp(8,8,8)
      double precision :: K_ee_qp(8,8,8)
      double precision :: K_ss_qp(8,8,8)
      double precision :: K_pt_qp(8,8,8)
      double precision :: K_ps_qp(8,8,8)
      double precision :: K_pd_qp(8,8,8)
      double precision :: K_ts_qp(8,8,8)
      double precision :: K_ep_qp(8,8,8)
      double precision :: K_es_qp(8,8,8)
      double precision :: K_st_qp(8,8,8)
      double precision :: K_se_qp(8,8,8)
      double precision :: K_sp_qp(8,8,8)
      double precision :: K_dd_qp(8,8,8)
      double precision :: K_ds_qp(8,8,8)
      double precision :: K_du_qp(8,8,24)
      double precision :: K_sd_qp(8,8,8)

      double precision :: res_u_e(24)
      double precision :: res_p_e(8)
      double precision :: res_t_e(8)
      double precision :: res_e_e(8)
      double precision :: res_d_e(8)
      double precision :: res_s_e(8)

      double precision :: K_uu_e(24,24)
      double precision :: K_us_e(24,8)
      double precision :: K_ue_e(24,8)
      double precision :: K_pu_e(8,24)
      double precision :: K_tu_e(8,24)
      double precision :: K_eu_e(8,24)
      double precision :: K_su_e(8,24)

      double precision :: K_pp_e(8,8)
      double precision :: K_tt_e(8,8)
      double precision :: K_ee_e(8,8)
      double precision :: K_ss_e(8,8)

      double precision :: K_pt_e(8,8)
      double precision :: K_ps_e(8,8)
      double precision :: K_pd_e(8,8)
      double precision :: K_ts_e(8,8)
      double precision :: K_ep_e(8,8)
      double precision :: K_es_e(8,8)
      double precision :: K_st_e(8,8)
      double precision :: K_se_e(8,8)
      double precision :: K_sp_e(8,8)

      double precision :: K_dd_e(8,8)
      double precision :: K_ds_e(8,8)
      double precision :: K_du_e(8,24)
      double precision :: K_sd_e(8,8)

      double precision :: sig(12,8)
      double precision :: eps(6,8)
      double precision :: shp(nel,8)
      double precision :: xsj(8)

      double precision :: Xn(8,3), Un(8,3)
      double precision :: unit_vec(3), w_mat(3,3)
      double precision :: rot_mat1(3,3), rot_mat2(3,3)
      double precision :: identity3(3,3)
      double precision :: x_qp, y_qp, z_qp  
      double precision,dimension(:,:),allocatable :: center_line
      integer :: n_cp, n_dim     
      character(128) :: file_name_CL
      double precision, dimension(3) ::node_X,
     &                                 longitudinal_vec,
     &                                 radial_vec,
     &                                 circumferential_vec,
     &                                 proj
      
      double precision :: resU(24), resP(8), resT(8), resE(8)
      double precision :: resD(8), resS(8)

      double precision :: tangUU(24,24), tangUS(24,8), tangUE(24,8)

      double precision :: tangPU(8,24), tangPP(8,8)
      double precision :: tangPT(8,8), tangPS(8,8), tangPD(8,8)

      double precision :: tangTU(8,24)
      double precision :: tangTT(8,8), tangTS(8,8)

      double precision :: tangEU(8,24), tangEP(8,8)
      double precision :: tangEE(8,8), tangES(8,8)

      double precision :: tangDU(8,24)
      double precision :: tangDD(8,8), tangDS(8,8)

      double precision :: tangSU(8,24), tangSP(8,8)
      double precision :: tangST(8,8), tangSE(8,8)
      double precision :: tangSD(8,8), tangSS(8,8)

      double precision :: detJac_current
      double precision :: c_p_iter_qp,c_t_iter_qp, c_e_iter_qp
      double precision :: c_d_iter_qp,rho_s_iter_qp, rho_E_iter_qp

      integer :: thread_id
!==========================================================================================================================!

      call PLBRK8(iel)  ! mesh visualization  

      if(isw.EQ.0) then
         if(ior.lt.0) then
            write(*,*) '3-d multiphysics element for ISR modeling'
         endif

      elseif (isw.EQ.1) then

       nel = 8          ! nodes per element for visualization
       pstyp = 3        ! mesh dimension for visualization

       if(ndf .NE. 12)then
       write(*,*)'elmt05: wrong value for ndf'
       write(*,*)'elmt05: ndf = 12 -> coupled growth problem'
       write(*,*)'elmt05: ndf = ',ndf
       write(*,*)'S T O P'
       stop
       endif

       ecapt(7) = 'PDGF [mol/mm3]'
       ecapt(8) = 'TGF beta [mol/mm3]'
       ecapt(9) = 'ECM [mol/mm3]'
       ecapt(10) = 'Drug [mol/mm3]'
       ecapt(11) = 'SMC [cells/mm3]'
       ecapt(12) = 'EC [cells/mm2]'

       errchk = pinput(d(1),8)
       if(errchk)then
          write(*,*)'pinput error'
       endif  

       errchk = pinput(d(9),3)
       if(errchk)then
          write(*,*)'pinput error'
       endif
       
       errchk = pinput(d(12),4)
       if(errchk)then
          write(*,*)'pinput error'
       endif
       
       errchk = pinput(d(16),3)
       if(errchk)then
          write(*,*)'pinput error'
       endif
       
       errchk = pinput(d(19),12)
       if(errchk)then
          write(*,*)'pinput error'
       endif
       
       errchk = pinput(d(31),6)
       if(errchk)then
          write(*,*)'pinput error'
       endif
       
       errchk = pinput(d(37),7)
       if(errchk)then
          write(*,*)'pinput error'
       endif
       
       do i=1,43
            write(iow,*)'Parameters: i =',i,'d(i) =',d(i)
       end do
       
       ! Activation of all DOFs for the active element except the EC density field
       ix(1) = 1 ! Ux
       ix(2) = 1 ! Uy
       ix(3) = 1 ! Uz
          
       ix(4) = 1 ! P
       ix(5) = 1 ! T
       ix(6) = 1 ! E
       ix(7) = 1 ! D
       ix(8) = 1 ! S   
       ix(9) = 0 ! En
       ix(10) = 0! fluid drug
       ix(11) = 0! TAWSS
       ix(12) = 0! OSI

       istv = max(istv,12)

      end if

!==========================================================================================================================!
      if ((isw.EQ.3).OR.(isw.EQ.4).OR.(isw.EQ.6).OR.(isw.EQ.8)) then
      
       ! Read in the center line file --------------------------------------------------------------------------------------!

       file_name_CL = 'IvoCenterlinesTXT.txt'

       call read_in_text_file(file_name_CL,center_line,n_cp,n_dim)
        
       ! Assignment of parameters-------------------------------------------------------------------------------------------!
       d_P = d(1)
       r_eta = d(2)
       eta_P = d(3)
       l_P1 = d(4)
       epsilon_p = d(5)
       l_P2 = d(6)
       c_T_th = d(7)
       c_P_healthy = d(8)  

       d_T = d(9)
       epsilon_T = d(10)
       c_T_healthy = d(11)

       eta_C = d(12)
       epsilon_C = d(13)
       c_C_threshold = d(14)
       c_C_healthy = d(15)

       d_D = d(16)
       epsilon_D1 = d(17)
       c_D_oneNM = d(18)  
       
       d_S = d(19)
       chi_S1 = d(20)
       chi_S2 = d(21)
       l_S1 = d(22)
       c_P_th = d(23)
       eta_S = d(24)
       l_S2 = d(25)
       l_S3 = d(26)
       A_S = d(27)
       B_S = d(28)
       alpha = d(29)
       rho_S_healthy = d(30)
       
       mu = d(31)
       lambda = d(32)
       k1_bar = d(33)
       k2 = d(34)
       kappa = d(35)
       gamma = d(36)
 
       flg_actv = INT(d(37))
       nqppd = INT(d(38))
       flg_cpl = INT(d(39))
       theta_numerical = d(40)
       flg_g = INT(d(41))
       flg_stab = INT(d(42))
       np_levels = INT(d(43))     

       k_P = c_P_healthy
       k_T = c_T_healthy
       k_C = c_C_healthy
       k_D = c_D_oneNM
       k_S = rho_S_healthy

       rho_E_healthy = 500.d00
       k_E = rho_E_healthy  
       
       ! Current and previous nodal values ---------------------------------------------------------------------------------!
       xu = xl + ul(1:3,:,1)

       c_p_iter_e = ul(4,:,1)
       c_t_iter_e = ul(5,:,1)
       c_e_iter_e = ul(6,:,1)
       c_d_iter_e = ul(7,:,1)
       rho_s_iter_e = ul(8,:,1)
       rho_E_iter_e = ul(9,:,1)

       c_p_prev_e = ul(4,:,1) - ul(4,:,2)
       c_t_prev_e = ul(5,:,1) - ul(5,:,2)
       c_e_prev_e = ul(6,:,1) - ul(6,:,2)
       c_d_prev_e = ul(7,:,1) - ul(7,:,2)
       rho_s_prev_e = ul(8,:,1) - ul(8,:,2)
       rho_E_prev_e = ul(9,:,1) - ul(9,:,2)

       ! Initializations ---------------------------------------------------------------------------------------------------!
       res_u_e = 0.d00
       res_p_e = 0.d00
       res_t_e = 0.d00
       res_e_e = 0.d00
       res_d_e = 0.d00
       res_s_e = 0.d00

       K_uu_e = 0.d00
       K_pp_e = 0.d00
       K_tt_e = 0.d00
       K_ee_e = 0.d00
       K_dd_e = 0.d00
       K_ss_e = 0.d00
       K_pt_e = 0.d00
       K_ps_e = 0.d00
       K_pd_e = 0.d00
       K_ts_e = 0.d00
       K_ep_e = 0.d00
       K_es_e = 0.d00
       K_ds_e = 0.d00
       K_sp_e = 0.d00
       K_st_e = 0.d00
       K_se_e = 0.d00
       K_sd_e = 0.d00
       K_pu_e = 0.d00
       K_tu_e = 0.d00
       K_eu_e = 0.d00
       K_du_e = 0.d00
       K_su_e = 0.d00
       K_us_e = 0.d00
       K_ue_e = 0.d00

       res_u_qp = 0.d00
       res_p_qp = 0.d00
       res_t_qp = 0.d00
       res_e_qp = 0.d00
       res_d_qp = 0.d00
       res_s_qp = 0.d00

       K_uu_qp = 0.d00
       K_pp_qp = 0.d00
       K_tt_qp = 0.d00
       K_ee_qp = 0.d00
       K_dd_qp = 0.d00
       K_ss_qp = 0.d00
       K_pt_qp = 0.d00
       K_ps_qp = 0.d00
       K_pd_qp = 0.d00
       K_ts_qp = 0.d00
       K_ep_qp = 0.d00
       K_es_qp = 0.d00
       K_ds_qp = 0.d00
       K_sp_qp = 0.d00
       K_st_qp = 0.d00
       K_se_qp = 0.d00
       K_sd_qp = 0.d00
       K_pu_qp = 0.d00
       K_tu_qp = 0.d00
       K_eu_qp = 0.d00
       K_du_qp = 0.d00
       K_su_qp = 0.d00
       K_us_qp = 0.d00
       K_ue_qp = 0.d00
       
       r = 0.d00
       
       s = 0.d00
         
         
         
        ! Copy nodal values -------------------------------------------------------------------------------------------------!
        Xn = TRANSPOSE(xl)
        Un = TRANSPOSE(ul(1:3,1:8,1))

        ! Generate integration quadrature -----------------------------------------------------------------------------------!       
        call int3d(nqppd,            
     &             nqp,              ! total number of quadrature points
     &             quadr)            ! integrature quadrature (isoparametric coordinates and weights)


        if (np_levels.eq.0) then     ! serial computation

        do qp = 1,nqp

        ! Isoparametric coordinates and weights for quadrature points -----------------------------------------------------!       
        xi = quadr(1,qp)                     
        eta = quadr(2,qp)
        zeta = quadr(3,qp)
        weight = quadr(4,qp)
   
        ! Evaluate shape function at the quadrature point -----------------------------------------------------------------!       
        call shape_func_eval3D(xi, eta, zeta, sf)
     
        ! Coordinates   
        x_qp = DOT_PRODUCT(sf,xl(1,:))
        y_qp = DOT_PRODUCT(sf,xl(2,:))
        z_qp = DOT_PRODUCT(sf,xl(3,:))
      
        ! DOF values  
        c_p_iter_qp = DOT_PRODUCT(sf,c_p_iter_e)
        c_t_iter_qp = DOT_PRODUCT(sf,c_t_iter_e)
        c_e_iter_qp = DOT_PRODUCT(sf,c_e_iter_e)
        c_d_iter_qp = DOT_PRODUCT(sf,c_d_iter_e)
        rho_s_iter_qp = DOT_PRODUCT(sf,rho_s_iter_e)
        
        ! Collagen orientation vectors ------------------------------------------------------------------------------------!

        node_X = 0.d00
        node_X(1) = x_qp
        node_X(2) = y_qp
        node_X(3) = z_qp
        
        call projection_onto_CL(node_X,
     &                          center_line,n_cp,n_dim,       
     &                          proj,
     &                          longitudinal_vec,
     &                          radial_vec)

        circumferential_vec = cross(longitudinal_vec,
     &                                  radial_vec)

        circumferential_vec = circumferential_vec/
     &                            norm2(circumferential_vec)

        ! Rodrigues formula
            
        w_mat = 0.d00
            
        w_mat(1,2) = -1.d00*radial_vec(3)
        w_mat(1,3) = radial_vec(2)
        w_mat(2,1) = radial_vec(3)
        w_mat(2,3) = -1.d00*radial_vec(1)
        w_mat(3,1) = -1.d00*radial_vec(2)
        w_mat(3,2) = radial_vec(1)
            
        identity3 = 0.d00
        identity3(1,1) = 1.d00
        identity3(2,2) = 1.d00
        identity3(3,3) = 1.d00
            
        rot_mat1 = identity3 + SIND(gamma)*w_mat +
     &      (2*(SIND(0.5d00*gamma))**2)*MATMUL(w_mat,w_mat)

        rot_mat2 = identity3 + SIND(-1.d00*gamma)*w_mat +
     &      (2*(SIND(-0.5d00*gamma))**2)*MATMUL(w_mat,w_mat)    

        e1 = MATMUL(rot_mat1,circumferential_vec)

        e2 = MATMUL(rot_mat2,circumferential_vec)

        e1 = (1/norm2(e1))*e1
        e2 = (1/norm2(e2))*e2

        ! Quadrature point residuals and tangent contributions ------------------------------------------------------------!
        ! Displacement DOFs ----------------------------------------------------------------------------------------!                              
        call elmt05_disp(flg_actv,
     &                   flg_cpl,   
     &                   flg_g,
     &                   theta_numerical,
     &                   Xn,
     &                   Un,
     &                   xi,
     &                   eta,
     &                   zeta,
     &                   e1,
     &                   e2,
     &                   mu,
     &                   lambda,
     &                   k1_bar,
     &                   k2,
     &                   kappa,
     &                   c_e_iter_e,
     &                   c_e_prev_e,
     &                   c_C_healthy,
     &                   k_C,
     &                   rho_s_iter_e,
     &                   rho_s_prev_e,
     &                   rho_S_healthy,
     &                   k_S,
     &                   detJac_current,
     &                   defgrad,
     &                   det_defgrad,
     &                   theta,
     &                   FPK,
     &                   resU,
     &                   tangUU,
     &                   tangUE,
     &                   tangUS)

        ! PDGF -----------------------------------------------------------------------------------------------------!
        call elmt05_pdgf(flg_actv,
     &                   flg_cpl,
     &                   theta_numerical,
     &                   dt,
     &                   Xn,
     &                   Un,
     &                   xi,
     &                   eta,
     &                   zeta,
     &                   c_p_iter_e,
     &                   c_p_prev_e,
     &                   d_P,
     &                   r_eta,
     &                   eta_P,
     &                   l_P1,
     &                   epsilon_P,
     &                   l_P2,
     &                   c_T_th,
     &                   k_P,
     &                   c_t_iter_e,
     &                   c_t_prev_e,
     &                   k_T,
     &                   c_d_iter_e,
     &                   c_d_prev_e,
     &                   k_D,
     &                   rho_s_iter_e,
     &                   rho_s_prev_e,
     &                   k_S,
     &                   resP,
     &                   tangPU,
     &                   tangPP,
     &                   tangPT,
     &                   tangPS,
     &                   tangPD)                              

        ! TGF-beta  -------------------------------------------------------------------------------------------------!
        call elmt05_tgf(flg_actv,
     &                  flg_cpl,
     &                  theta_numerical,
     &                  dt,
     &                  Xn,
     &                  Un,
     &                  xi,
     &                  eta,
     &                  zeta,
     &                  c_t_iter_e,
     &                  c_t_prev_e,
     &                  d_T,
     &                  epsilon_T,
     &                  k_T,
     &                  rho_s_iter_e,
     &                  rho_s_prev_e,
     &                  k_S,
     &                  resT,
     &                  tangTU,
     &                  tangTT,
     &                  tangTS)

        ! ECM  -------------------------------------------------------------------------------------------------!
        call elmt05_ecm(flg_actv,
     &                  flg_cpl,
     &                  theta_numerical,
     &                  dt,
     &                  Xn,
     &                  Un,
     &                  xi,
     &                  eta,
     &                  zeta,
     &                  c_p_iter_e,
     &                  c_p_prev_e,
     &                  k_P,
     &                  c_e_iter_e,
     &                  c_e_prev_e,
     &                  eta_C,
     &                  epsilon_C,
     &                  c_C_healthy,
     &                  c_C_threshold/c_C_healthy,
     &                  k_C,
     &                  rho_s_iter_e,
     &                  rho_s_prev_e,
     &                  k_S,
     &                  resE,
     &                  tangEU,
     &                  tangEP,
     &                  tangEE,
     &                  tangES) 

        ! drug  ------------------------------------------------------------------------------------------------!
        call elmt05_drug(flg_actv,
     &                   flg_cpl,
     &                   theta_numerical,
     &                   dt,
     &                   Xn,
     &                   Un,
     &                   xi,
     &                   eta,
     &                   zeta,
     &                   c_d_iter_e,
     &                   c_d_prev_e,
     &                   d_D,
     &                   epsilon_D1,
     &                   k_D,
     &                   rho_s_iter_e,
     &                   rho_s_prev_e,
     &                   k_S,
     &                   resD,
     &                   tangDU,
     &                   tangDD,
     &                   tangDS)  

        ! SMC  -------------------------------------------------------------------------------------------------!
        call elmt05_smc(flg_actv,
     &                  flg_cpl,
     &                  flg_stab,
     &                  theta_numerical,
     &                  dt,
     &                  Xn,
     &                  Un,
     &                  xi,
     &                  eta,
     &                  zeta,
     &                  c_p_iter_e,
     &                  c_p_prev_e,
     &                  k_P,
     &                  c_t_iter_e,
     &                  c_t_prev_e,
     &                  k_T,
     &                  c_e_iter_e,
     &                  c_e_prev_e,
     &                  k_C,
     &                  c_d_iter_e,
     &                  c_d_prev_e,
     &                  k_D,
     &                  rho_s_iter_e,
     &                  rho_s_prev_e,
     &                  d_S,
     &                  chi_S2,
     &                  c_C_threshold/c_C_healthy,
     &                  c_C_healthy,
     &                  chi_S1,
     &                  l_S1,
     &                  c_P_th,
     &                  eta_S,
     &                  l_S2,
     &                  l_S3,
     &                  c_T_th,
     &                  A_S,
     &                  B_S,
     &                  alpha,
     &                  k_S,
     &                  rho_E_iter_e,
     &                  rho_E_prev_e,
     &                  rho_E_healthy,
     &                  k_E,
     &                  resS,
     &                  tangSU,
     &                  tangSP,
     &                  tangST,
     &                  tangSE,
     &                  tangSD,
     &                  tangSS)     
    

        cauchy = (1/det_defgrad)*MATMUL(defgrad,TRANSPOSE(FPK))      ! Cauchy stress computation
        call voigt2(cauchy,cauchy_v)                                 ! Voigt notation

        ! Copy plot variables into the stress array for visualization -----------------------------------------------------!
        ! (all the quantities in the current configuration)
        sig(1:6,qp) = cauchy_v                     ! Cauchy stress components in Voigt notation
        sig(7,qp) = c_p_iter_qp*k_P/det_defgrad    ! PDGF concentration
        sig(8,qp) = c_t_iter_qp*k_T/det_defgrad    ! TGF-beta concentration
        sig(9,qp) = c_e_iter_qp*k_C /det_defgrad   ! Collagen concentration
        sig(10,qp) = c_d_iter_qp*k_D/det_defgrad   ! drug concentration
        sig(11,qp) = rho_s_iter_qp*k_S/det_defgrad ! SMC density

        ! Store time history plot data for element ------------------------------------------------------------------------!  
        i = 12*(qp-1)
        do j = 1,11
           tt(j+i) = sig(j,qp)
        end do

        ! Store Jacobian determinant and shape function for visualization -------------------------------------------------!  
        xsj(qp) = detJac_current
        shp(1:8,qp) = sf

        if ((isw.EQ.3).OR.(isw.EQ.6)) then

         ! Assign residual vector values ----------------------------------------------------------------------------------!  
         res_u_qp(qp,:) = resU*weight
         res_p_qp(qp,:) = resP*weight
         res_t_qp(qp,:) = resT*weight
         res_e_qp(qp,:) = resE*weight
         res_d_qp(qp,:) = resD*weight
         res_s_qp(qp,:) = resS*weight

         if (isw.EQ.3) then 
            ! Assign element matrix values --------------------------------------------------------------------------------!  
            K_uu_qp(qp,:,:) = tangUU*weight
            K_us_qp(qp,:,:) = tangUS*weight
            K_ue_qp(qp,:,:) = tangUE*weight
            K_pu_qp(qp,:,:) = tangPU*weight
            K_pp_qp(qp,:,:) = tangPP*weight
            K_pt_qp(qp,:,:) = tangPT*weight
            K_ps_qp(qp,:,:) = tangPS*weight
            K_pd_qp(qp,:,:) = tangPD*weight
            K_tu_qp(qp,:,:) = tangTU*weight
            K_tt_qp(qp,:,:) = tangTT*weight
            K_ts_qp(qp,:,:) = tangTS*weight
            K_eu_qp(qp,:,:) = tangEU*weight
            K_ep_qp(qp,:,:) = tangEP*weight
            K_ee_qp(qp,:,:) = tangEE*weight
            K_es_qp(qp,:,:) = tangES*weight
            K_du_qp(qp,:,:) = tangDU*weight
            K_dd_qp(qp,:,:) = tangDD*weight
            K_ds_qp(qp,:,:) = tangDS*weight
            K_su_qp(qp,:,:) = tangSU*weight
            K_sp_qp(qp,:,:) = tangSP*weight
            K_st_qp(qp,:,:) = tangST*weight
            K_se_qp(qp,:,:) = tangSE*weight
            K_sd_qp(qp,:,:) = tangSD*weight
            K_ss_qp(qp,:,:) = tangSS*weight
         end if             
        end if        
        
        end do

        else         ! parallel computation

        ! Set open MP threads = total number of quadrature points -----------------------------------------------------------!       
        call omp_set_num_threads(nqp)

        ! Enable nested parallelism -----------------------------------------------------------------------------------------!
        call omp_set_max_active_levels(np_levels)

! Initialize quadrature point openMP fork -----------------------------------------------------------------------------------!       
!$omp parallel private(qp,xi,eta,zeta,weight,
!$   * sf,x_qp,y_qp,z_qp,c_p_iter_qp,c_t_iter_qp,
!$   * c_e_iter_qp,c_d_iter_qp,rho_s_iter_qp,
!$   * unit_vec,w_mat,identity3,node_X,
!$   * proj,longitudinal_vec,circumferential_vec,radial_vec,                     
!$   * rot_mat1,rot_mat2,e1,e2,detJac_current,defgrad, 
!$   * det_defgrad,theta,FPK,cauchy,resU,resP,resT,resE,resD, 
!$   * resS,tangUU,tangUE,tangUS,tangPU,tangPP, 
!$   * tangPT,tangPS,tangPD,tangTU,tangTT,tangTS,tangEU, 
!$   * tangEP,tangEE,tangES,tangDU,tangDD,tangDS,tangSU, 
!$   * tangSP,tangST,tangSE,tangSD,tangSS) 
!$   * shared(sig,tt,xsj,shp,res_u_qp,res_p_qp,res_t_qp,
!$   * center_line,n_cp,n_dim,file_name_CL,
!$   * res_e_qp,res_d_qp,res_s_qp,K_uu_qp,K_us_qp,K_ue_qp,
!$   * K_pu_qp,K_pp_qp,K_pt_qp,K_ps_qp,K_pd_qp,K_tu_qp,
!$   * K_tt_qp,K_ts_qp,K_eu_qp,K_ep_qp,K_ee_qp,K_es_qp,
!$   * K_du_qp,K_dd_qp,K_ds_qp,K_su_qp,K_sp_qp,K_st_qp,
!$   * K_se_qp,K_sd_qp,K_ss_qp)       

        qp = omp_get_thread_num()+1             ! Quadrature point identifier = thread rank + 1 (since ranks start from 0)
        !write(*,*) 'Quadrature point = ',qp
        !write(*,*) 'level = ',omp_get_level() 
      
        ! Isoparametric coordinates and weights for quadrature points -----------------------------------------------------!       
        xi = quadr(1,qp)                     
        eta = quadr(2,qp)
        zeta = quadr(3,qp)
        weight = quadr(4,qp)
   
        ! Evaluate shape function at the quadrature point -----------------------------------------------------------------!       
        call shape_func_eval3D(xi, eta, zeta, sf)
     
        ! Coordinates   
        x_qp = DOT_PRODUCT(sf,xl(1,:))
        y_qp = DOT_PRODUCT(sf,xl(2,:))
        z_qp = DOT_PRODUCT(sf,xl(3,:))
      
        ! DOF values  
        c_p_iter_qp = DOT_PRODUCT(sf,c_p_iter_e)
        c_t_iter_qp = DOT_PRODUCT(sf,c_t_iter_e)
        c_e_iter_qp = DOT_PRODUCT(sf,c_e_iter_e)
        c_d_iter_qp = DOT_PRODUCT(sf,c_d_iter_e)
        rho_s_iter_qp = DOT_PRODUCT(sf,rho_s_iter_e)
        
        ! Collagen orientation vectors ------------------------------------------------------------------------------------!          

        node_X = 0.d00
        node_X(1) = x_qp
        node_X(2) = y_qp
        node_X(3) = z_qp

        call projection_onto_CL(node_X,
     &                          center_line,n_cp,n_dim,       
     &                          proj,
     &                          longitudinal_vec,
     &                          radial_vec)

        circumferential_vec = cross(longitudinal_vec,
     &                                  radial_vec)

        circumferential_vec = circumferential_vec/
     &                            norm2(circumferential_vec)

        ! Rodrigues formula
            
        w_mat = 0.d00
            
        w_mat(1,2) = -1.d00*radial_vec(3)
        w_mat(1,3) = radial_vec(2)
        w_mat(2,1) = radial_vec(3)
        w_mat(2,3) = -1.d00*radial_vec(1)
        w_mat(3,1) = -1.d00*radial_vec(2)
        w_mat(3,2) = radial_vec(1)
            
        identity3 = 0.d00
        identity3(1,1) = 1.d00
        identity3(2,2) = 1.d00
        identity3(3,3) = 1.d00
            
        rot_mat1 = identity3 + SIND(gamma)*w_mat +
     &      (2*(SIND(0.5d00*gamma))**2)*MATMUL(w_mat,w_mat)

        rot_mat2 = identity3 + SIND(-1.d00*gamma)*w_mat +
     &      (2*(SIND(-0.5d00*gamma))**2)*MATMUL(w_mat,w_mat)    

        e1 = MATMUL(rot_mat1,circumferential_vec)

        e2 = MATMUL(rot_mat2,circumferential_vec)

        e1 = (1/norm2(e1))*e1
        e2 = (1/norm2(e2))*e2

        ! Quadrature point residuals and tangent contributions ------------------------------------------------------------!
! Initialize DOF openMP fork ----------------------------------------------------------------------------------------------!
!$omp parallel              
!$omp sections
!$omp section
        ! Displacement DOFs thread ----------------------------------------------------------------------------------------!
        !write(*,*) 'Displacement DOF, thread = ',omp_get_thread_num()
        !write(*,*) 'level = ',omp_get_level()                               
        call elmt05_disp(flg_actv,
     &                   flg_cpl,   
     &                   flg_g,
     &                   theta_numerical,
     &                   Xn,
     &                   Un,
     &                   xi,
     &                   eta,
     &                   zeta,
     &                   e1,
     &                   e2,
     &                   mu,
     &                   lambda,
     &                   k1_bar,
     &                   k2,
     &                   kappa,
     &                   c_e_iter_e,
     &                   c_e_prev_e,
     &                   c_C_healthy,
     &                   k_C,
     &                   rho_s_iter_e,
     &                   rho_s_prev_e,
     &                   rho_S_healthy,
     &                   k_S,
     &                   detJac_current,
     &                   defgrad,
     &                   det_defgrad,
     &                   theta,
     &                   FPK,
     &                   resU,
     &                   tangUU,
     &                   tangUE,
     &                   tangUS)

!$omp section
        ! PDGF thread -----------------------------------------------------------------------------------------------------!
        !write(*,*) 'PDGF DOF, thread = ',omp_get_thread_num()
        !write(*,*) 'level = ',omp_get_level()        
        call elmt05_pdgf(flg_actv,
     &                   flg_cpl,
     &                   theta_numerical,
     &                   dt,
     &                   Xn,
     &                   Un,
     &                   xi,
     &                   eta,
     &                   zeta,
     &                   c_p_iter_e,
     &                   c_p_prev_e,
     &                   d_P,
     &                   r_eta,
     &                   eta_P,
     &                   l_P1,
     &                   epsilon_P,
     &                   l_P2,
     &                   c_T_th,
     &                   k_P,
     &                   c_t_iter_e,
     &                   c_t_prev_e,
     &                   k_T,
     &                   c_d_iter_e,
     &                   c_d_prev_e,
     &                   k_D,
     &                   rho_s_iter_e,
     &                   rho_s_prev_e,
     &                   k_S,
     &                   resP,
     &                   tangPU,
     &                   tangPP,
     &                   tangPT,
     &                   tangPS,
     &                   tangPD)                              

!$omp section
        ! TGF-beta thread -------------------------------------------------------------------------------------------------!
        !write(*,*) 'TGF DOF, thread = ',omp_get_thread_num()
        !write(*,*) 'level = ',omp_get_level() 
        call elmt05_tgf(flg_actv,
     &                  flg_cpl,
     &                  theta_numerical,
     &                  dt,
     &                  Xn,
     &                  Un,
     &                  xi,
     &                  eta,
     &                  zeta,
     &                  c_t_iter_e,
     &                  c_t_prev_e,
     &                  d_T,
     &                  epsilon_T,
     &                  k_T,
     &                  rho_s_iter_e,
     &                  rho_s_prev_e,
     &                  k_S,
     &                  resT,
     &                  tangTU,
     &                  tangTT,
     &                  tangTS)

!$omp section
        ! ECM thread -------------------------------------------------------------------------------------------------!
        !write(*,*) 'ECM DOF, thread = ',omp_get_thread_num()
        !write(*,*) 'level = ',omp_get_level() 
        call elmt05_ecm(flg_actv,
     &                  flg_cpl,
     &                  theta_numerical,
     &                  dt,
     &                  Xn,
     &                  Un,
     &                  xi,
     &                  eta,
     &                  zeta,
     &                  c_p_iter_e,
     &                  c_p_prev_e,
     &                  k_P,
     &                  c_e_iter_e,
     &                  c_e_prev_e,
     &                  eta_C,
     &                  epsilon_C,
     &                  c_C_healthy,
     &                  c_C_threshold/c_C_healthy,
     &                  k_C,
     &                  rho_s_iter_e,
     &                  rho_s_prev_e,
     &                  k_S,
     &                  resE,
     &                  tangEU,
     &                  tangEP,
     &                  tangEE,
     &                  tangES) 

!$omp section
        ! drug thread ------------------------------------------------------------------------------------------------!
        !write(*,*) 'drug DOF, thread = ',omp_get_thread_num()
        !write(*,*) 'level = ',omp_get_level() 
        call elmt05_drug(flg_actv,
     &                   flg_cpl,
     &                   theta_numerical,
     &                   dt,
     &                   Xn,
     &                   Un,
     &                   xi,
     &                   eta,
     &                   zeta,
     &                   c_d_iter_e,
     &                   c_d_prev_e,
     &                   d_D,
     &                   epsilon_D1,
     &                   k_D,
     &                   rho_s_iter_e,
     &                   rho_s_prev_e,
     &                   k_S,
     &                   resD,
     &                   tangDU,
     &                   tangDD,
     &                   tangDS)  

!$omp section
        ! SMC thread -------------------------------------------------------------------------------------------------!
        !write(*,*) 'SMC DOF, thread = ',omp_get_thread_num()
        !write(*,*) 'level = ',omp_get_level() 
        call elmt05_smc(flg_actv,
     &                  flg_cpl,
     &                  flg_stab,
     &                  theta_numerical,
     &                  dt,
     &                  Xn,
     &                  Un,
     &                  xi,
     &                  eta,
     &                  zeta,
     &                  c_p_iter_e,
     &                  c_p_prev_e,
     &                  k_P,
     &                  c_t_iter_e,
     &                  c_t_prev_e,
     &                  k_T,
     &                  c_e_iter_e,
     &                  c_e_prev_e,
     &                  k_C,
     &                  c_d_iter_e,
     &                  c_d_prev_e,
     &                  k_D,
     &                  rho_s_iter_e,
     &                  rho_s_prev_e,
     &                  d_S,
     &                  chi_S2,
     &                  c_C_threshold/c_C_healthy,
     &                  c_C_healthy,
     &                  chi_S1,
     &                  l_S1,
     &                  c_P_th,
     &                  eta_S,
     &                  l_S2,
     &                  l_S3,
     &                  c_T_th,
     &                  A_S,
     &                  B_S,
     &                  alpha,
     &                  k_S,
     &                  rho_E_iter_e,
     &                  rho_E_prev_e,
     &                  rho_E_healthy,
     &                  k_E,
     &                  resS,
     &                  tangSU,
     &                  tangSP,
     &                  tangST,
     &                  tangSE,
     &                  tangSD,
     &                  tangSS)     
!$omp end sections
!$omp end parallel     
! Merge DOF openMP fork ---------------------------------------------------------------------------------------------------!       

        cauchy = (1/det_defgrad)*MATMUL(defgrad,TRANSPOSE(FPK))      ! Cauchy stress computation
        call voigt2(cauchy,cauchy_v)                                 ! Voigt notation

        ! Copy plot variables into the stress array for visualization -----------------------------------------------------!
        ! (all the quantities in the current configuration)
        sig(1:6,qp) = cauchy_v                     ! Cauchy stress components in Voigt notation
        sig(7,qp) = c_p_iter_qp*k_P/det_defgrad    ! PDGF concentration
        sig(8,qp) = c_t_iter_qp*k_T/det_defgrad    ! TGF-beta concentration
        sig(9,qp) = c_e_iter_qp*k_C /det_defgrad    ! Collagen concentration
        sig(10,qp) = c_d_iter_qp*k_D/det_defgrad   ! drug concentration
        sig(11,qp) = rho_s_iter_qp*k_S/det_defgrad ! SMC density

        ! Store time history plot data for element ------------------------------------------------------------------------!  
        i = 12*(qp-1)
        do j = 1,11
           tt(j+i) = sig(j,qp)
        end do

        ! Store Jacobian determinant and shape function for visualization -------------------------------------------------!  
        xsj(qp) = detJac_current
        shp(1:8,qp) = sf

        if ((isw.EQ.3).OR.(isw.EQ.6)) then

         ! Assign residual vector values ----------------------------------------------------------------------------------!  
         res_u_qp(qp,:) = resU*weight
         res_p_qp(qp,:) = resP*weight
         res_t_qp(qp,:) = resT*weight
         res_e_qp(qp,:) = resE*weight
         res_d_qp(qp,:) = resD*weight
         res_s_qp(qp,:) = resS*weight

         if (isw.EQ.3) then 
            ! Assign element matrix values --------------------------------------------------------------------------------!  
            K_uu_qp(qp,:,:) = tangUU*weight
            K_us_qp(qp,:,:) = tangUS*weight
            K_ue_qp(qp,:,:) = tangUE*weight
            K_pu_qp(qp,:,:) = tangPU*weight
            K_pp_qp(qp,:,:) = tangPP*weight
            K_pt_qp(qp,:,:) = tangPT*weight
            K_ps_qp(qp,:,:) = tangPS*weight
            K_pd_qp(qp,:,:) = tangPD*weight
            K_tu_qp(qp,:,:) = tangTU*weight
            K_tt_qp(qp,:,:) = tangTT*weight
            K_ts_qp(qp,:,:) = tangTS*weight
            K_eu_qp(qp,:,:) = tangEU*weight
            K_ep_qp(qp,:,:) = tangEP*weight
            K_ee_qp(qp,:,:) = tangEE*weight
            K_es_qp(qp,:,:) = tangES*weight
            K_du_qp(qp,:,:) = tangDU*weight
            K_dd_qp(qp,:,:) = tangDD*weight
            K_ds_qp(qp,:,:) = tangDS*weight
            K_su_qp(qp,:,:) = tangSU*weight
            K_sp_qp(qp,:,:) = tangSP*weight
            K_st_qp(qp,:,:) = tangST*weight
            K_se_qp(qp,:,:) = tangSE*weight
            K_sd_qp(qp,:,:) = tangSD*weight
            K_ss_qp(qp,:,:) = tangSS*weight
         end if             
        end if

!$omp end parallel
! Merge quadrature point openMP fork --------------------------------------------------------------------------------------!       

      end if
      end if
      !read(*,*)
      
      if ((isw.EQ.3).OR.(isw.EQ.6)) then

        do i = 1, nqp
         ! Assemble residual vectors --------------------------------------------------------------------------------------!  
         res_u_e = res_u_e + res_u_qp(i,:)
         res_p_e = res_p_e + res_p_qp(i,:)
         res_t_e = res_t_e + res_t_qp(i,:)
         res_e_e = res_e_e + res_e_qp(i,:)
         res_d_e = res_d_e + res_d_qp(i,:)
         res_s_e = res_s_e + res_s_qp(i,:)
     
         if (isw.EQ.3) then        
            ! Assemble element matrices -----------------------------------------------------------------------------------!  
            K_uu_e = K_uu_e + K_uu_qp(i,:,:)
            K_us_e = K_us_e + K_us_qp(i,:,:)
            K_ue_e = K_ue_e + K_ue_qp(i,:,:)
            K_pu_e = K_pu_e + K_pu_qp(i,:,:)
            K_pp_e = K_pp_e + K_pp_qp(i,:,:)
            K_pt_e = K_pt_e + K_pt_qp(i,:,:)
            K_ps_e = K_ps_e + K_ps_qp(i,:,:)
            K_pd_e = K_pd_e + K_pd_qp(i,:,:)
            K_tu_e = K_tu_e + K_tu_qp(i,:,:)
            K_tt_e = K_tt_e + K_tt_qp(i,:,:)
            K_ts_e = K_ts_e + K_ts_qp(i,:,:)
            K_eu_e = K_eu_e + K_eu_qp(i,:,:)
            K_ep_e = K_ep_e + K_ep_qp(i,:,:)
            K_ee_e = K_ee_e + K_ee_qp(i,:,:)
            K_es_e = K_es_e + K_es_qp(i,:,:)
            K_du_e = K_du_e + K_du_qp(i,:,:)
            K_dd_e = K_dd_e + K_dd_qp(i,:,:)
            K_ds_e = K_ds_e + K_ds_qp(i,:,:)
            K_su_e = K_su_e + K_su_qp(i,:,:)
            K_sp_e = K_sp_e + K_sp_qp(i,:,:)
            K_st_e = K_st_e + K_st_qp(i,:,:)
            K_se_e = K_se_e + K_se_qp(i,:,:)
            K_sd_e = K_sd_e + K_sd_qp(i,:,:)
            K_ss_e = K_ss_e + K_ss_qp(i,:,:)     
         end if

        end do

      end if

      if ((isw.EQ.3).OR.(isw.EQ.6)) then

       ! Assemble residual vectors --------------------------------------------------------------------------------------!  
       r(1:24) =  res_u_e
       r(25:32) = res_p_e
       r(33:40) = res_t_e
       r(41:48) = res_e_e
       r(49:56) = res_d_e
       r(57:64) = res_s_e

       ! Sort residual vector based on DOF arrangement -----------------------------------------------------------------!  
       call FeapCoupledSortVec3D(r)

       r = -1.d00*r
       
       if (isw.EQ.3) then

         ! Assemble element matrices -----------------------------------------------------------------------------------!  
         select case (flg_cpl)
       
         case (0) ! Staggered solution
         
            s(1:24,1:24) =   K_uu_e
            s(25:32,25:32) = K_pp_e
            s(33:40,33:40) = K_tt_e
            s(41:48,41:48) = K_ee_e
            s(49:56,49:56) = K_dd_e
            s(57:64,57:64) = K_ss_e

            ! Sort element matrix based on DOF arrangement ----------------------------------------------------------------!  
            call FeapCoupledSortMat3D(s)
        
         case (1) ! Monolithic solution
       
            s(1:24,1:24)   = K_uu_e

            s(25:32,1:24)  = K_pu_e
            s(33:40,1:24)  = K_tu_e
            s(41:48,1:24)  = K_eu_e
            s(49:56,1:24)  = K_du_e
            s(57:64,1:24)  = K_su_e

            s(25:32,25:32) = K_pp_e
            s(41:48,25:32) = K_ep_e
            s(57:64,25:32) = K_sp_e  
            
            s(25:32,33:40) = K_pt_e
            s(33:40,33:40) = K_tt_e
            s(57:64,33:40) = K_st_e

            s(1:24, 41:48) = K_ue_e
            s(41:48,41:48) = K_ee_e
            s(57:64,41:48) = K_se_e

            s(25:32,49:56) = K_pd_e
            s(49:56,49:56) = K_dd_e
            s(57:64,49:56) = K_sd_e

            s(1:24,57:64)  = K_us_e
            s(25:32,57:64) = K_ps_e
            s(33:40,57:64) = K_ts_e
            s(41:48,57:64) = K_es_e
            s(49:56,57:64) = K_ds_e
            s(57:64,57:64) = K_ss_e

            ! Sort element matrix based on DOF arrangement ----------------------------------------------------------------!  
            call FeapCoupledSortMat3D(s)

         end select
        
       end if
        
      end if

      ! Plotting operations -----------------------------------------------------------------------------------------------!  
      if ((isw.EQ.4).OR.(isw.EQ.8)) then
       lint = 8
       nnl = 8
       call slcn3d_mod(sig,
     &                 shp,
     &                 xsj,
     &                 quadr,
     &                 lint,
     &                 nnl,
     &                 r,
     &                 s)
      end if
      ! Plotting operations -----------------------------------------------------------------------------------------------!  

      end subroutine elmt05
      
         
