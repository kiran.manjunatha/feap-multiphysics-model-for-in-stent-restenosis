        module fiber_alignment
        
        contains
        
        
        subroutine read_in_text_file(file_name,read_in_array,rows,cols)

            character(128), intent(in) :: file_name
            character(128):: buffer

            integer :: strlen, rows, cols, i, io
            double precision, dimension(:,:),
     &                        allocatable :: read_in_array


            open (1, file = file_name, status='old', action='read')

            !Count the number of columns

            read(1,'(a)') buffer !read first line WITH SPACES INCLUDED
            rewind(1) !Get back to the file beginning

            strlen = len(buffer) !Find the REAL length of a string read
            do while (buffer(strlen:strlen) == ' ') 
                strlen = strlen - 1 
            enddo

            cols=0 !Count the number of spaces in the first line
            do i=0,strlen
                if (buffer(i:i) == ',') then
                    cols=cols+1
                endif
            enddo

            cols = cols + 1

            !Count the number of rows

            rows = 0 !Count the number of lines in a file
            do
                read(1,*,iostat=io)
                if (io/=0) exit
                rows = rows + 1
            end do

            rewind(1)

            !write(*,*) 'rows: ', rows
            !write(*,*) 'cols: ', cols

            allocate(read_in_array(rows,cols))

            do i=1,rows,1
                read(1,*) read_in_array(i,:)
            enddo

            close (1)

        end subroutine read_in_text_file


        subroutine projection_onto_CL(node_X,
     &                                center_line,n_cp,n_dim,
     &                                proj,long_vec,rad_vec)

            double precision,intent(in),
     &      dimension(n_cp,n_dim):: center_line
            double precision,dimension(3),intent(in) :: node_X
            double precision,dimension(3) :: proj_vec
            double precision,dimension(3),intent(out) :: proj
            
            integer :: i

            double precision, dimension(3) :: line_vec,rad_vec,long_vec

            double precision :: dist, min_dist

            double precision,dimension(3) :: proj_temp,rad_vec_temp


            !   A---D-------------B
            !    .  .
            !     . .
            !       C

            !AD = AB * (AB dot AC) / (AB dot AB)
            !D = (dx, dy) = A + AD
            
            min_dist = 100.d00
            do i = 1,n_cp-1
                line_vec = center_line(i+1,:) - center_line(i,:)

                if ((dot_product(line_vec,
     &             (node_X-center_line(i,:))).gt.1.d-16).and.
     &             (dot_product(-1.d00*line_vec,
     &             (node_X-center_line(i+1,:)))).gt.1.d-16) then
                    proj_vec = (dot_product(line_vec,
     &                         (node_X-center_line(i,:)))/
     &                         dot_product(line_vec,line_vec))*line_vec
                    proj_temp = center_line(i,:) + proj_vec

                    rad_vec_temp = node_X - proj_temp
                    dist = norm2(rad_vec_temp)

                else if(abs(dot_product(line_vec,
     &             (node_X-center_line(i,:)))).le.0.1) then
                    rad_vec_temp = node_X-center_line(i,:)
                    dist = norm2(rad_vec_temp)
                    proj_temp = center_line(i,:)
                else if(abs(dot_product(-1.d00*line_vec,
     &             (node_X-center_line(i+1,:)))).le.0.1) then
                    rad_vec_temp = node_X-center_line(i+1,:)
                    dist = norm2(rad_vec_temp)
                    proj_temp = center_line(i+1,:)
                end if

                if ((dist.gt.0.d00).and.(dist.lt.min_dist)) then
                    min_dist = dist
                    proj = proj_temp
                    rad_vec = rad_vec_temp/norm2(rad_vec_temp)
                    long_vec = line_vec/norm2(line_vec)
                end if
                
            end do
            


        end subroutine projection_onto_CL


        function cross(a, b)
            double precision, dimension(3) :: cross
            double precision, dimension(3), intent(in) :: a, b
          
            cross(1) = a(2) * b(3) - a(3) * b(2)
            cross(2) = a(3) * b(1) - a(1) * b(3)
            cross(3) = a(1) * b(2) - a(2) * b(1)
        end function cross 
        
        
        end module fiber_alignment
