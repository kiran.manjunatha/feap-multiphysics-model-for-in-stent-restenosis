        module fiber_alignment
        
        contains
        
        
        subroutine read_in_text_file(file_name,read_in_array,rows,cols)

            character(128), intent(in) :: file_name
            character(128):: buffer

            integer :: strlen, rows, cols, i, io
            double precision, dimension(:,:),
     &                        allocatable :: read_in_array


            open (1, file = file_name, status='old', action='read')

            !Count the number of columns

            read(1,'(a)') buffer !read first line WITH SPACES INCLUDED
            rewind(1) !Get back to the file beginning

            strlen = len(buffer) !Find the REAL length of a string read
            do while (buffer(strlen:strlen) == ' ') 
                strlen = strlen - 1 
            enddo

            cols=0 !Count the number of spaces in the first line
            do i=0,strlen
                if (buffer(i:i) == ',') then
                    cols=cols+1
                endif
            enddo

            cols = cols + 1

            !Count the number of rows

            rows = 0 !Count the number of lines in a file
            do
                read(1,*,iostat=io)
                if (io/=0) exit
                rows = rows + 1
            end do

            rewind(1)

            !write(*,*) 'rows: ', rows
            !write(*,*) 'cols: ', cols

            allocate(read_in_array(rows,cols))

            do i=1,rows,1
                read(1,*) read_in_array(i,:)
            enddo

            close (1)

        end subroutine read_in_text_file


        subroutine projection_onto_CL(node_X,
     &                                center_line,n_cp,n_dim,
     &                                proj,long_vec,rad_vec)

            double precision,intent(in),
     &      dimension(n_cp,n_dim):: center_line
            double precision,dimension(3),intent(in) :: node_X
            double precision,dimension(3) :: proj_vec
            double precision,dimension(3),intent(out) :: proj
            
            integer :: i

            double precision, dimension(3) :: line_vec,rad_vec,long_vec

            double precision :: dist, min_dist

            double precision,dimension(3) :: proj_temp,rad_vec_temp


            !   A---D-------------B
            !    .  .
            !     . .
            !       C

            !AD = AB * (AB dot AC) / (AB dot AB)
            !D = (dx, dy) = A + AD
            
            min_dist = 100.d00
            do i = 1,n_cp-1
                line_vec = center_line(i+1,:) - center_line(i,:)

                if ((dot_product(line_vec,
     &             (node_X-center_line(i,:))).gt.1.d-16).and.
     &             (dot_product(-1.d00*line_vec,
     &             (node_X-center_line(i+1,:)))).gt.1.d-16) then
                    proj_vec = (dot_product(line_vec,
     &                         (node_X-center_line(i,:)))/
     &                         dot_product(line_vec,line_vec))*line_vec
                    proj_temp = center_line(i,:) + proj_vec

                    rad_vec_temp = node_X - proj_temp
                    dist = norm2(rad_vec_temp)

                else if(abs(dot_product(line_vec,
     &             (node_X-center_line(i,:)))).le.0.1) then
                    rad_vec_temp = node_X-center_line(i,:)
                    dist = norm2(rad_vec_temp)
                    proj_temp = center_line(i,:)
                else if(abs(dot_product(-1.d00*line_vec,
     &             (node_X-center_line(i+1,:)))).le.0.1) then
                    rad_vec_temp = node_X-center_line(i+1,:)
                    dist = norm2(rad_vec_temp)
                    proj_temp = center_line(i+1,:)
                end if

                if ((dist.gt.0.d00).and.(dist.lt.min_dist)) then
                    min_dist = dist
                    proj = proj_temp
                    rad_vec = rad_vec_temp/norm2(rad_vec_temp)
                    long_vec = line_vec/norm2(line_vec)
                end if
                
            end do
            


        end subroutine projection_onto_CL


        function cross(a, b)
            double precision, dimension(3) :: cross
            double precision, dimension(3), intent(in) :: a, b
          
            cross(1) = a(2) * b(3) - a(3) * b(2)
            cross(2) = a(3) * b(1) - a(1) * b(3)
            cross(3) = a(1) * b(2) - a(2) * b(1)
        end function cross 
        
        
        end module fiber_alignment
    

      
      subroutine shape_func_eval3D(xi, eta, zeta, N)
         
         DOUBLE PRECISION, INTENT(IN)  :: xi, eta, zeta
         DOUBLE PRECISION, INTENT(OUT) :: N(8)
         DOUBLE PRECISION :: N0(8),N_xi(8),N_eta(8),N_zeta(8)
         DOUBLE PRECISION :: N_xi_eta(8),N_eta_zeta(8)
         double precision :: N_zeta_xi(8),N_xi_eta_zeta(8)

         N0 = (/1,1,1,1,1,1,1,1/)

         N_xi = (/-1,1,1,-1,-1,1,1,-1/)
         N_eta = (/-1,-1,1,1,-1,-1,1,1/)
         N_zeta = (/-1,-1,-1,-1,1,1,1,1/)

         N_xi_eta = (/1,-1,1,-1,1,-1,1,-1/)
         N_eta_zeta = (/1,1,-1,-1,-1,-1,1,1/)
         N_zeta_xi = (/1,-1,-1,1,-1,1,1,-1/)

         N_xi_eta_zeta = (/-1,1,-1,1,1,-1,1,-1/)

         N = (1.d0/8)*(N0 
     &                + xi*N_xi 
     &                + eta*N_eta 
     &                + zeta*N_zeta 
     &                + xi*eta*N_xi_eta 
     &                + eta*zeta*N_eta_zeta 
     &                + zeta*xi*N_zeta_xi 
     &                + xi*eta*zeta*N_xi_eta_zeta)

      return
      end subroutine shape_func_eval3D







      subroutine shape_func_eval2D(xi, eta, N)
         
         DOUBLE PRECISION, INTENT(IN)  :: xi, eta
         DOUBLE PRECISION, INTENT(OUT) :: N(4)
         DOUBLE PRECISION :: N0(4),N_xi(4),N_eta(4)
         DOUBLE PRECISION :: N_xi_eta(4)

         N0 = (/1,1,1,1/)

         N_xi = (/-1,1,1,-1/)
         N_eta = (/-1,-1,1,1/)

         N_xi_eta = (/1,-1,1,-1/)

         N = (1.d0/4)*(N0 
     &                + xi*N_xi 
     &                + eta*N_eta 
     &                + xi*eta*N_xi_eta)

      return
      end subroutine shape_func_eval2D



!############################################
!     
      subroutine FeapCoupledSortVec3D(vec)   
      implicit none   
      double precision, intent(inout) :: vec(96)   
      integer :: i
      integer :: j

      double precision :: vecw(96)
      integer, parameter :: pos(96) = [
     &     1 ,  2,  3, 25, 33, 41, 49, 57, 65, 73, 81, 89,
     &     4 ,  5,  6, 26, 34, 42, 50, 58, 66, 74, 82, 90,
     &     7 ,  8,  9, 27, 35, 43, 51, 59, 67, 75, 83, 91,
     &     10, 11, 12, 28, 36, 44, 52, 60, 68, 76, 84, 92,
     &     13, 14, 15, 29, 37, 45, 53, 61, 69, 77, 85, 93,
     &     16, 17, 18, 30, 38, 46, 54, 62, 70, 78, 86, 94,
     &     19, 20, 21, 31, 39, 47, 55, 63, 71, 79, 87, 95,
     &     22, 23, 24, 32, 40, 48, 56, 64, 72, 80, 88, 96]   
      vecw = vec   

      do i=1,96
         vec(i) = vecw(pos(i))
      enddo   
      return   
      end subroutine FeapCoupledSortVec3D
   
!    ##########################################
     
            
      subroutine FeapCoupledSortMat3D(mat)   
      implicit none   
      double precision, intent(inout) :: mat(96,96)   
      integer :: i
      integer :: j

      double precision :: matw(96,96)
      integer, parameter :: pos(96) = [
     &     1 ,  2,  3, 25, 33, 41, 49, 57, 65, 73, 81, 89,
     &     4 ,  5,  6, 26, 34, 42, 50, 58, 66, 74, 82, 90,
     &     7 ,  8,  9, 27, 35, 43, 51, 59, 67, 75, 83, 91,
     &     10, 11, 12, 28, 36, 44, 52, 60, 68, 76, 84, 92,
     &     13, 14, 15, 29, 37, 45, 53, 61, 69, 77, 85, 93,
     &     16, 17, 18, 30, 38, 46, 54, 62, 70, 78, 86, 94,
     &     19, 20, 21, 31, 39, 47, 55, 63, 71, 79, 87, 95,
     &     22, 23, 24, 32, 40, 48, 56, 64, 72, 80, 88, 96]    
      matw = mat   

      do i=1,96
         do j=1,96
            mat(i,j) = matw(pos(i),pos(j))
         enddo
      enddo   
      return   
      end subroutine FeapCoupledSortMat3D
      !
      !############################################

!    ##########################################

 
      



      subroutine voigt2(a,a_voigt)

         implicit none
     
         double precision :: a(3,3), a_voigt(6)
         integer :: i
     
         do i = 1,3
             a_voigt(i) = a(i,i)
         end do
     
         a_voigt(4) = a(1,2)
         a_voigt(5) = a(1,3)
         a_voigt(6) = a(2,3)
         return
         end subroutine voigt2
   



      
      subroutine slcn3d_mod(sig,shp,jac,quadr,lint,nnl, p,s)

!      * * F E A P * * A Finite Element Analysis Program

!....  Copyright (c) 1984-2020: Regents of the University of California
!                               All rights reserved

!      Purpose: Project 3-d element variables to bulk nodes

!      Inputs:
!        sig(10,*)    - Stresses at quadrature points
!        eps( 6,*)    - Strains  at quadrature points
!        lint         - Number of quadrature points
!        nnl          - Number nodes on element

!      Outputs:
!        p(nen)       - Weights for 'lumped' projection
!        s(nen,*)     - Integral of variables
!-----[--.----+----.----+----.-----------------------------------------]
      implicit   none

      include   'cdata.h'
      include   'elcapt.h'
      include   'eldatp.h'
      include   'eldata.h'
      include   'iofile.h'
      include   'strnum.h'
      include   'pointer.h'
      include   'comblk.h'

      integer       :: ii, i,j,l, lint, nnl
      double precision :: p(*),s(nen,*), sig(12,*)
      double precision :: xj, quadr(4,*)

!     Local arrays
      double precision :: jac(8), shp(8,8)
      
      save


!     !  Initialize arrays
        do ii = 1,nnl
          p(ii)      = 0.0d0
          s(ii,1:11) = 0.0d0
        end do ! ii
      
!       Compute projections: int ( sig * shp(i) * darea )
        do l = 1,lint
          do ii = 1,nnl
            xj         = jac(l)*shp(ii,l)*quadr(4,l)
            p(ii)      = p(ii)      + xj
            s(ii,1:11 ) = s(ii,1:11 ) + sig(1:11,l)*xj
          end do ! ii
        end do ! l

      !endif


!     Set number of plots
      iste = 12
      
      

      end subroutine slcn3d_mod







      subroutine slcn3dsurf_mod(sig,shp,jac,quadr,lint,nnl, p,s)

         !      * * F E A P * * A Finite Element Analysis Program
         
         !....  Copyright (c) 1984-2020: Regents of the University of California
         !                               All rights reserved
         
         !      Purpose: Project 3-d element variables to surface nodes
         
         !      Inputs:
         !        sig(10,*)    - Stresses at quadrature points
         !        eps( 6,*)    - Strains  at quadrature points
         !        lint         - Number of quadrature points
         !        nnl          - Number nodes on element
         
         !      Outputs:
         !        p(nen)       - Weights for 'lumped' projection
         !        s(nen,*)     - Integral of variables
         !-----[--.----+----.----+----.-----------------------------------------]
               implicit   none
         
               include   'cdata.h'
               include   'elcapt.h'
               include   'eldatp.h'
               include   'eldata.h'
               include   'iofile.h'
               include   'strnum.h'
               include   'pointer.h'
               include   'comblk.h'
         
               integer       :: ii, i,j,l, lint, nnl
               double precision :: p(*),s(nen,*), sig(12,*)
               double precision :: xj, quadr(3,*)
         
         !     Local arrays         
               double precision :: jac(4), shp(4,4)
               
               save
         
         
         !     !  Initialize arrays
                 do ii = 1,nnl
                   p(ii)   = 0.0d0
                   s(ii,12) = 0.0d0
                 end do ! ii
               
         !       Compute projections: int ( sig * shp(i) * darea )
                 do l = 1,lint
                   do ii = 1,nnl
                     xj         = jac(l)*shp(ii,l)*quadr(3,l)
                     p(ii)      = p(ii)      + xj
                     s(ii,12) = s(ii,12) + sig(12,l)*xj
                   end do ! ii
                 end do ! l
         
               !endif
         
         
         !     Set number of plots
               iste = 12
               
               
         
               end subroutine slcn3dsurf_mod























         subroutine findelem(array,
     &                    nrows,
     &                    ncols,
     &          target_elements,
     &               target_len,
     &                    found,
     &              row_indices)

            implicit none
            integer, intent(in) :: nrows, ncols, target_len
            integer, intent(in), dimension(nrows,ncols) :: array
            integer, intent(in), 
     &             dimension(target_len) :: target_elements
            logical, dimension(2), intent(out) :: found
            integer, intent(out), dimension(2) :: row_indices
            integer :: elemcount
          
            integer :: i, j, k, match_count
          
            found = .false.
            row_indices = 0
            elemcount = 0
            do i = 1, nrows
              match_count = 0
              do j = 1, ncols
                do k = 1, target_len
                  if (array(i,j) == target_elements(k)) then
                    match_count = match_count + 1
                    exit
                  endif
                enddo
              enddo
          
              if (match_count == target_len) then
                elemcount = elemcount + 1
                found(elemcount) = .true.
                row_indices(elemcount) = i
              endif
            enddo

          end subroutine findelem



         subroutine findelem_bound(array,
     &              nrows,
     &              ncols,
     &              target_elements,
     &              target_len,
     &              found1,
     &              found2,
     &              row_indices1,
     &              row_indices2,
     &              col_indices1t,
     &              col_indices1r,
     &              col_indices2t,
     &              col_indices2r)

            implicit none
            integer, intent(in) :: nrows, ncols, target_len
            integer, intent(in), dimension(nrows,ncols) :: array
            integer, intent(in), 
     &             dimension(target_len) :: target_elements
            logical, dimension(4), intent(out) :: found1, found2
            integer, intent(out),
     &        dimension(4) :: row_indices1, row_indices2
            integer :: col_indicest(2), col_indicesr(2) 
            integer, intent(out) :: col_indices1t(4),col_indices1r(4)
            integer, intent(out) :: col_indices2t(4,2),
     &                              col_indices2r(4,2)
            integer :: elem1count, elem2count
          
            integer :: i, j, k, match_count
          
            found1 = .false.
            found2 = .false.
            
            row_indices1 = 0
            row_indices2 = 0

            elem1count = 0
            elem2count = 0

            col_indices1t = 0
            col_indices1r = 0
            col_indices2t = 0
            col_indices2r = 0
            do i = 1, nrows
              match_count = 0
              col_indicest = 0
              col_indicesr = 0
              do j = 1, ncols
                do k = 1, target_len
                  if (array(i,j) == target_elements(k)) then
                    match_count = match_count + 1
                    col_indicest(match_count) = k
                    col_indicesr(match_count) = j
                    exit
                  endif
                enddo
              enddo
          
              if (match_count == 2) then
                elem2count = elem2count + 1
                found2(elem2count) = .true.
                row_indices2(elem2count) = i
                col_indices2t(elem2count,1:2) = col_indicest(1:2)
                col_indices2r(elem2count,1:2) = col_indicesr(1:2)
              elseif (match_count == 1) then
                elem1count = elem1count + 1
                found1(elem1count) = .true.
                row_indices1(elem1count) = i
                col_indices1t(elem1count) = col_indicest(1)
                col_indices1r(elem1count) = col_indicesr(1)               
              endif
            enddo

          end subroutine findelem_bound


        subroutine exclude_elements(ref_array,
     &                               size_ref,     
     &                            input_array,
     &                               size_inp,
     &                           output_array,
     &                               n_output)


        integer, intent(in) :: size_ref, size_inp  
        integer, intent(in), dimension(size_ref) :: ref_array
        integer, intent(in), dimension(size_inp) :: input_array
        integer, intent(out), dimension(size_ref-size_inp)
     &                      :: output_array
        integer, intent(out) :: n_output
        integer :: i, j
        logical :: found
        integer, allocatable :: temp_array(:)
        integer :: n_temp

        n_temp = 0
        allocate(temp_array(size(ref_array)))

        outer_loop: do i = 1, size(ref_array)
            found = .false.
            inner_loop: do j = 1, size(input_array)
                if (ref_array(i).eq.input_array(j)) then
                    found = .true.
                    exit inner_loop
                end if
            end do inner_loop
            if (.not. found) then
                n_temp = n_temp + 1
                temp_array(n_temp) = ref_array(i)
            end if
        end do outer_loop

        n_output = n_temp
        output_array(1:n_output) = temp_array(1:n_output)

        deallocate(temp_array)

      end subroutine exclude_elements



      subroutine cyclic_move(arr, n, shift_amount)
         integer, intent(inout) :: arr(n)
         integer, intent(in) :: n, shift_amount
         integer :: temp_arr(n)
         integer :: i, new_idx
 
         ! Create a temporary array to store shifted elements
         do i = 1, n
             new_idx = mod(i - 1 + shift_amount, n) + 1
             temp_arr(new_idx) = arr(i)
         end do
 
         ! Copy the temporary array back to the original array
         arr = temp_arr
 
      end subroutine cyclic_move
