# FEAP - multiphysics model for in-stent restenosis 

## Steps to compile serial FEAP
1. Clone the repository into the FEAP directory
2. Run make from within either of the subdirectories cloned (multithreaded building can be enabled via the command make -j #, # being the number of threads)
3. An executable is generated within the subdirectory which you can run after creating a suitable input file as shown in the user manual


## DOI for citing the work
10.5281/zenodo.10100652

## Steps to compile parfeap

download petsc-3.17.5 from https://ftp.mcs.anl.gov/pub/petsc/ 
(October 2022 release)

tar -xvf petsc-3.17.5

cd petsc-3.17.5

module purge

module load GCCcore/.11.3.0 

module load GCCcore/.12.2.0

module load CMake/3.24.3

module load intel/2022b

module load GCC/11.3.0

module load GCC/12.2.0

module load intel-compilers/2022.1.0

module load intel-compilers/2022.2.1

### bash file additions
export FEAPHOME8_6=/PATH/TO/code-8.6.1o

export PETSC_DIR=/PATH/TO/petsc-3.17.5

export PETSC_ARCH=real-intelmpi2022

### configuring petsc

./configure --with-clean --with-ssl=0 --with-c++-support --with-debugging=0 --with-shared-libraries=0 --download-metis --download-parmetis --download-superlu_dist --download-superlu --download-mumps --download-blacs --download-fblaslapack --with-mpi-compilers=1 --with-scalar-type=real PETSC_ARCH=real-intelmpi2022 --download-scalapack --with-mpi-dir=$I_MPI_ROOT

### installing petsc

make PETSC_DIR=/PATH/TO/petsc PETSC_ARCH=real-intelmpi2022 all

make PETSC_DIR=/PATH/TO/petsc PETSC_ARCH=real-intelmpi2022 test

### installing parfeap

cd $FEAPHOME8_6

make install !!!!(intel version)

cd parfeap

make install

### running parfeap

mpirun -np 4 ./feap -ksp_monitor_short -ksp_converged_reason -ksp_type preonly -pc_type lu -pc_factor_mat_solver_type mumps -memory_info -malloc_log -log_view
