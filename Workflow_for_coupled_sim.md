# FEAP - XNS coupled simulation workflow
Files needed:
(a) ABAQUS ".inp" file containing the discretized geometry of the stented artery
IMPORTANT: 
1. There must be overlapping surface elements on the luminal side of the artery
2. The luminal surface elements must be conformal to the stent geometry where the stent is in apposition with the artery
3. The luminal surface elements must be categorized into stent-appositional regions (artery_stented) and non appositiona regions (artery_free)
4. The bulk elements must be categorized into media (solidmap_media) and adventitia (solidmap_adventitia)

All the component names can be adjusted in the file 'utilities/ABQtoFEAP/github_repo/readinp_no_stent.m'

(b) A text file containing the discrete center line points along the length of the artery


Steps:
1. Copy the text file with disctete center line points in the directory 'utilities/fiber_alignment_outer_adventitia'
2. Adjust the file name of the center line points in 'fiber_orientation.f', compile using gfortran or ifort, and run the executable generated
3. Run the MATLAB file 'utilities/ABQtoFEAP/ABQtoFEAP.m' which will generate all the input files needed for FEAP in an additional folder called 'FEAP'
4. Update the center line text file name in the file 'e05_multiphysics.f' located either in 'version_with_X11' or 'version_without_X11', and compile the FEAP executable
5. Copy the FEAP executable and the center line text file into the 'FEAP' folder generated (step 3)
6. Copy  the 'FEAP' folder to wherever you want to run the simulation

