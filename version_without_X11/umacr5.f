!$Id:$
      subroutine umacr5(lct,ctl)

!      * * F E A P * * A Finite Element Analysis Program

!....  Copyright (c) 1984-2020: Regents of the University of California
!                               All rights reserved

!-----[--.----+----.----+----.-----------------------------------------]
!     Modification log                                Date (dd/mm/year)
!       Original version                                    01/11/2006
!       1. Remove 'prt' from argument list                  09/07/2009
!       2. Add 'uhelpfl' comment option                     19/10/2017
!       3. Add 'help' comment option - replaces 'uhelpfl'   07/01/2019
!-----[--.----+----.----+----.-----------------------------------------]
!      Purpose:  User interface for adding solution command language
!                instructions.

!      Inputs:
!         lct       - Command character parameters
!         ctl(3)    - Command numerical parameters

!      Outputs:
!         N.B.  Users are responsible for command actions.  See
!               programmers manual for example.
!-----[--.----+----.----+----.-----------------------------------------]

      implicit  none

      include  'iofile.h'
      include  'umac1.h'
      include  'sdata.h'  ! ndf, ndm, nst, n_el
      include  'cdata.h'  ! numel
      include  'eldata.h'
      include  'pointer.h'! np(*) pointers
      include  'comblk.h' ! mr(*) and hr(*) arrays
      include  'cdat1.h'  ! ndd, nie 
      include  'region.h'
      include  'hdata.h'  ! history pointers
      
      character (len=15) :: lct

      logical       :: pcomp
      real (kind=8) :: ctl(3)

      integer :: i, j, k, found, n_unique
      integer, allocatable :: temp_unique_array(:)
      logical :: is_unique
      integer :: nodel2(numel,16)
      integer :: elemn2(numel)
      integer, dimension(:,:), allocatable :: nodelist2
      integer, dimension(:), allocatable :: elemnum2
      integer :: fluxel, matel
      integer :: npel2

      integer, dimension(:), allocatable :: bound_node_list
      character(len=36) :: file_name = "fbnodalnumbers.txt"
      integer :: unit_number

      save

!     Set command word
      
      if(pcomp(uct,'mac5',4)) then      ! Usual    form
        
        uct = 'fbnn'                     ! Specify 'name'

      elseif(pcomp(lct,'help',4)) then  ! Write help information

        write(*,*) 'COMMAND: fbnn'
        write(*,*) 'prints flux-boundary nodal numbers'

      elseif(urest.eq.1) then           ! Read  restart data

      elseif(urest.eq.2) then           ! Write restart data

      else                              ! Perform user operation

      endif

      fluxel = 0
      npel2 = 4
      
      if (numel.gt.0) then

      do i = 1,numel
            matel = mr(np(33) + (i*nen1)-1)
            if ((matel.eq.3).or.(matel.eq.4)) then
                  fluxel = fluxel + 1
                  nodel2(fluxel,:) = 
     &            mr(np(33)+(i-1)*nen1:np(33)+(i-1)*nen1+npel2)
                  elemn2(fluxel) = i
            end if
      end do 

      allocate(nodelist2(fluxel,npel2))
      allocate(elemnum2(fluxel))

      nodelist2(1:fluxel,1:npel2) = nodel2(1:fluxel,1:npel2)
      elemnum2 = elemn2(1:fluxel)

      if (fluxel.gt.0) then

      allocate(temp_unique_array(fluxel*npel2))

      n_unique = 0
      do i = 1, fluxel
            do j = 1, npel2
                is_unique = .true.
                do k = 1, n_unique
                if (nodelist2(i, j) == temp_unique_array(k)) then
                    is_unique = .false.
                    exit
                endif
                end do

                if (is_unique) then
                    n_unique = n_unique + 1
                    temp_unique_array(n_unique) = nodelist2(i, j)
                endif
            end do
      end do

      allocate(bound_node_list(n_unique))
      bound_node_list = temp_unique_array(1:n_unique)

      open(newunit=unit_number, file=file_name,
     &status="replace", action="write")
        
      do i = 1, n_unique
        write(unit_number, "(I10,2x,F20.14,2x,F20.14,2x,F20.14)") 
     &   bound_node_list(i), 
     &   hr(np(43) + bound_node_list(i)*ndm -3),
     &   hr(np(43) + bound_node_list(i)*ndm -2),
     &   hr(np(43) + bound_node_list(i)*ndm -1)
      end do
        
      close(unit_number)

      end if

      deallocate (nodelist2,elemnum2,temp_unique_array)
      deallocate(bound_node_list,elemnum2)

      end if

      end subroutine umacr5
                
