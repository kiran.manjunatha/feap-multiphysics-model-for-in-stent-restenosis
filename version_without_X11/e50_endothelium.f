!$Id:$
      subroutine elmt50(
     &                  d,                   ! element data parameters
     &                  ul,                  ! element nodal solution parameters
     &                  xl,                  ! element nodal reference coordinates
     &                  ix,                  ! element global node numbers
     &                  tl,                  ! element nodal temperature values
     &                  s,                   ! element matrix (e.g. stiffness, mass)
     &                  r,                   ! element vector (e.g. residual, mass)
     &                  ndf,                 ! number of unknowns (max per node)
     &                  ndm,                 ! space dimension of mesh
     &                  nst,                 ! size of element arrays s and r
     &                  isw)                 ! task parameter to control computation


!      * * F E A P * * A Finite Element Analysis Program

!....  Copyright (c) 1984-2020: Regents of the University of California
!                               All rights reserved

!-----[--.----+----.----+----.-----------------------------------------]
!     Modification log                                Date (dd/mm/year)
!       Original version                                    01/11/2006
!       Multiphysics surface element                        17/10/2023
!-----[--.----+----.----+----.-----------------------------------------]
!     Author:  Kiran Manjunatha (kiran.manjunatha@ifam.rwth-aachen.de)

!     Purpose: A multiphysics surface element for in-stent restenosis
!              Element with all DOFs activated      

!-----[--.----+----.----+----.-----------------------------------------] 

      use omp_lib               ! open MP parallelization 

      implicit  none

      include 'bdata.h'
      include 'cdata.h'
      include 'eldata.h'
      include 'elplot.h'
      include 'elcapt.h'
      include 'hdata.h'
      include 'iofile.h'
      include 'rdata.h'
      include 'prstrs.h'
      include 'pdata6.h' 
      include 'strnum.h'
      include 'tdata.h'
      include 'eltran.h'
      include 'augdat.h'
      include 'comblk.h'
      include 'pointer.h'

      double precision :: d(*)            ! Material set parameters
      double precision :: ul(ndf,nen,6)   ! Solution parameters
      double precision :: xl(ndm,nen)     ! Element nodal coordinates
      integer :: ix(*)                    ! Element global node numbers      
      double precision :: tl(*)           ! Element vector (e.g., nodal temperatures)
      integer :: ndf                      ! Maximum no dof's/node
      integer :: ndm                      ! Mesh spatial dimension
      integer :: nst                      ! Element matrix/vector dimension
      integer :: isw                      ! Switch parameter (set by input commands)

      double precision :: s(nst,nst)      ! Element matrix (stiffness, mass, etc.)
      double precision :: r(nst)          ! Element vector (residual, lump mass, etc.)
      double precision :: p(nen)
      
      logical :: pinput, tinput, errchk

!==================================================Parameters==============================================================!
! Growth factors (GF)------------------------------------------------------------------------------------------------------!
! Platelet-derived growth factor (PDGF)------------------------------------------------------------------------------------!
      double precision :: q_P_bar_ref           ! peak PDGF influx scaled by the non-dimensionalization factor
      double precision :: l_B_P                 ! exponential factor for PDGF influx

! Transforming growth factor (TGF)-----------------------------------------------------------------------------------------!
      double precision :: q_T_bar_ref           ! peak TGF-beta influx scaled by the non-dimensionalization factor
      double precision :: l_B_T                 ! exponential factor for TGF-beta influx

! Drug (rapamycin)---------------------------------------------------------------------------------------------------------!
      double precision :: q_D_bar_ref           ! peak drug influx in [NM]
      double precision :: t_p                   ! curvature parameter 1 for drug influx (time to achieve peak influx)
      double precision :: t_c                   ! curvature parameter 2 for drug influx
      double precision :: epsilon_D2            ! receptor internalization coefficient on ECs
      double precision :: Pdrug                 ! permeability of endothelium to drug
      double precision :: c_D_oneNM             ! one [NM] concentration of drug in [mol/mm^3]
      double precision :: k_D                   ! scaling for non-dimensionalition (=c_D_oneNM) 

! Endothelial cells (EC) --------------------------------------------------------------------------------------------------!
      double precision :: d_E                   ! EC diffusivity
      double precision :: eta_E                 ! EC proliferation coefficient
      double precision :: A_E                   ! maximum efficacy of the drug against EC proliferation
      double precision :: B_E                   ! drug concentration in [NM] for half efficacy
      double precision :: beta                  ! Hill coefficient for drug-induced EC proliferation inhibition
      double precision :: epsilon_E             ! EC apoptosis coefficient
      double precision :: l_E                   ! drug-dose-dependent EC apoptosis parameter
      double precision :: rho_E_healthy         ! EC density on healthy endothelia
      double precision :: s_E                   ! TAWSS and OSI dependent EC shape index parameter
      double precision :: k_E                   ! scaling for non-dimensionalition (=rho_E_healthy)

! Simulation parameters ---------------------------------------------------------------------------------------------------!
      integer :: nqppd                          ! number of quadrature points per dimension
      integer :: flg_sapp                       ! flag to identify stent-apposition surface
      integer :: np_levels                      ! levels of nested parallelism enabled   
      
!==========================================================================================================================!

      integer :: qp
      integer :: i, nqp, lint, nnl

      double precision :: quadr(3,4)

      double precision :: f_B2, f_B3
      double precision :: f_B3_temp
      double precision :: max_lf_d, A_D
      double precision :: lfP, lfT
      double precision :: sigma_d
      double precision :: c_d_wall_iter_e(4), c_d_wall_iter_qp
      double precision :: c_d_iter_e(4), c_d_iter_qp

      double precision :: rho_E_iter_e(4), rho_E_iter_qp
      double precision :: rho_E_prev_e(4), rho_E_prev_qp

      double precision :: fE1, fE2

      double precision :: osi_iter_e(4), osi_iter_qp
      double precision :: tawss_iter_e(4), tawss_iter_qp

      double precision :: si_E

      double precision :: res_p_qp(4,4), res_t_qp(4,4)
      double precision :: res_d_qp(4,4), res_en_qp(4,4)
      double precision :: rp(4), rt(4), rd(4), ren(4)

      double precision :: kpu_qp(4,4,12),ktu_qp(4,4,12)
      double precision :: kdu_qp(4,4,12), kpen_qp(4,4,4)
      double precision :: kten_qp(4,4,4), kenu_qp(4,4,12)
      double precision :: kend_qp(4,4,4), kenen_qp(4,4,4)
      double precision :: kdd_qp(4,4,4), kden_qp(4,4,4)

      double precision :: kpu(4,12),ktu(4,12), kdu(4,12)
      double precision :: kpen(4,4),kten(4,4)
      double precision :: kenu(4,12),kend(4,4),kenen(4,4)
      double precision :: kdd(4,4), kden(4,4)

      double precision :: resP(4), resT(4), resD1(4) 
      double precision :: resD2(4), resEn(4)

      double precision :: tangPU(4,12), tangPEn(4,4)
      double precision :: tangTU(4,12), tangTEn(4,4)
      double precision :: tangD1U(4,12), tangD2U(4,12)
      double precision :: tangEnU(4,12),tangEnD(4,4),tangEnEn(4,4)
      double precision :: tangD1D(4,4), tangD1En(4,4)
      double precision :: tangD2D(4,4), tangD2En(4,4)

      double precision :: xu(ndm,nen)

      double precision :: detJac,detJaccur
      double precision :: defgrad(3,3), det_defgrad

      double precision :: xi, eta, zeta, weight
      double precision :: sf(4)

      double precision :: sig(12,4)
      double precision :: shp(nel,4)
      double precision :: xsj(4)
      double precision :: Xn(4,3), Un(4,3)

!==========================================================================================================================!

      call PLQUD4(iel)  ! mesh visualization

      if(isw.eq.0) then
        if(ior.lt.0) then
          write(*,*) '3-d Surface - Species Flux + Endothelium'
        endif

      elseif (isw.EQ.1) then

         nel = 4        ! nodes per element for visualization
         pstyp = 2      ! mesh dimension for visualization

         if(ndf .NE. 12)then
              write(*,*)'elmt50: wrong value for ndf'
              write(*,*)'elmt50: ndf = 12 -> coupled growth problem'
              write(*,*)'elmt50: ndf = ',ndf
              write(*,*)'S T O P'
              stop
         endif

         ecapt(1) = 'rho_ec[cells/mm2]'

         errchk = pinput(d(1),2)
         if(errchk)then
            write(*,*)'pinput error'
         endif  

         errchk = pinput(d(3),2)
         if(errchk)then
            write(*,*)'pinput error'
         endif
        
         errchk = pinput(d(5),6)
         if(errchk)then
            write(*,*)'pinput error'
         endif

         errchk = pinput(d(11),9)
         if(errchk)then
            write(*,*)'pinput error'
         endif

         errchk = pinput(d(20),3)
         if(errchk)then
            write(*,*)'pinput error'
         endif         

         do i=1,22
              write(iow,*)'Parameters: i =',i,'d(i) =',d(i)
         end do

         ! Activation of all DOFs for the element 
         ix(1) = 1 ! Ux
         ix(2) = 1 ! Uy
         ix(3) = 1 ! Uz
            
         ix(4) = 1 ! P
         ix(5) = 1 ! T
         ix(6) = 1 ! E
         ix(7) = 1 ! D
         ix(8) = 1 ! S   
         ix(9) = 1 ! En
         ix(10) = 1! fluid_drug
         ix(11) = 1! TAWSS
         ix(12) = 1! OSI

         istv = max(istv,12)
         
      end if
      
      q_P_bar_ref = d(1)
      l_B_P = d(2)

      q_T_bar_ref = d(3)
      l_B_T = d(4)

      q_D_bar_ref = d(5)
      t_p = d(6)
      t_c = d(7)
      epsilon_D2 = d(8)
      Pdrug = d(9)
      c_D_oneNM = d(10)

      d_E = d(11)
      eta_E = d(12)
      A_E = d(13)
      B_E = d(14)
      beta = d(15)
      epsilon_E = d(16)
      l_E = d(17)
      rho_E_healthy = d(18)
      s_E = d(19)

      k_D = c_D_oneNM
      k_E = rho_E_healthy
      
      nqppd = int(d(20))
      flg_sapp = int(d(21))
      np_levels = int(d(22))                  


      if(isw.eq.3 .or. isw.eq.6 .or. isw.eq.4 .or. isw.eq.8) then

       ! Current and previous nodal values ---------------------------------------------------------------------------------!
        xu = xl + ul(1:3,1:4,1)
               
        c_d_iter_e = ul(7,1:4,1)
        rho_E_iter_e = ul(9,1:4,1)
        c_d_wall_iter_e = ul(10,1:4,1)
        tawss_iter_e = ul(11,1:4,1)
        osi_iter_e = ul(12,1:4,1)

        rho_E_prev_e = ul(9,1:4,1) - ul(9,1:4,2)

        ! Calculations for drug load factor --------------------------------------------------------------------------------!
        f_B3_temp = t_c*exp(t_p/t_c) - t_c

        max_lf_d = f_B3_temp*(t_c**(t_c/f_B3_temp))*
     &  (f_B3_temp+t_c)**(-(f_B3_temp + t_c)/f_B3_temp)

        A_D = 1/max_lf_d

        !write(*,*) 'f_B3_temp =',f_B3_temp
        !write(*,*) 'max_lf_d =',max_lf_d
        !write(*,*) 'A_D =',A_D
        

       ! Initializations ---------------------------------------------------------------------------------------------------!
        kpu = 0.d00
        ktu = 0.d00
        kdu = 0.d00
        kenu = 0.d00

        kpen = 0.d00
        kten = 0.d00
        kden = 0.d00

        kdd = 0.d00

        kend = 0.d00
        kenen = 0.d00
        
        rp = 0.d00
        rt = 0.d00
        rd = 0.d00
        ren = 0.d00

        res_p_qp = 0.d00
        res_t_qp = 0.d00
        res_d_qp = 0.d00
        res_en_qp = 0.d00

        kpu_qp = 0.d00
        ktu_qp = 0.d00
        kdu_qp = 0.d00
        kenu_qp = 0.d00

        kpen_qp = 0.d00
        kten_qp = 0.d00
        kden_qp = 0.d00

        kdd_qp = 0.d00

        kend_qp = 0.d00
        kenen_qp = 0.d00

        ! Copy nodal values -------------------------------------------------------------------------------------------------!
        Xn = TRANSPOSE(xl(1:3,1:4))
        Un = TRANSPOSE(ul(1:3,1:4,1)) 

        ! Generate integration quadrature -----------------------------------------------------------------------------------!       
        call int2d(nqppd,            ! number of quadrature points per dimension
     &             nqp,              ! total number of quadrature points
     &             quadr)            ! integrature quadrature (isoparametric coordinates and weights)
        
        if (np_levels.eq.0) then     ! serial computation

        do qp = 1,nqp
        ! Isoparametric coordinates and weights for quadrature points -------------------------------------------------------!
        xi = quadr(1,qp)
        eta = quadr(2,qp)
        zeta = 0.d00
        weight = quadr(3,qp)   
        
        ! Evaluate shape function at the quadrature point -------------------------------------------------------------------! 
        call shape_func_eval2D(xi, eta, sf)

        ! DOF values
        rho_E_iter_qp = dot_product(sf,rho_E_iter_e)
        rho_E_prev_qp = dot_product(sf,rho_E_prev_e)
        c_d_wall_iter_qp = dot_product(sf,c_d_wall_iter_e)
        tawss_iter_qp = dot_product(sf,tawss_iter_e)
        osi_iter_qp = dot_product(sf,osi_iter_e)
        
        ! Quadrature point residuals and tangent contributions --------------------------------------------------------------!
        ! PDGF+TGF-beta ----------------------------------------------------------------------------------------------!

        ! EC-shape index calculation
        si_E = max(0.05d00,
     &   exp(-s_E*((1.d00 - osi_iter_qp)**4)*tawss_iter_qp))

        call elmt50_lumen_pdgf_tgf(dt,
     &                             ttim,
     &                             Xn,
     &                             Un,
     &                             xi,
     &                             eta,
     &                             zeta,
     &                             rho_E_iter_e,
     &                             k_E,
     &                             rho_E_healthy,
     &                             si_E,
     &                             q_P_bar_ref,
     &                             l_B_P,
     &                             q_T_bar_ref,
     &                             l_B_T,
     &                             lfP,
     &                             lfT,
     &                             resP,
     &                             tangPU,
     &                             tangPEn,
     &                             resT,
     &                             tangTU,
     &                             tangTEn)                            


        ! EC ---------------------------------------------------------------------------------------------------------!
        call elmt50_lumen_EC(dt,
     &                       Xn,
     &                       Un,
     &                       xi,
     &                       eta,
     &                       zeta,
     &                       c_d_iter_e,
     &                       k_D,
     &                       rho_E_iter_e,
     &                       rho_E_prev_e,
     &                       d_E,
     &                       eta_E,
     &                       A_E,
     &                       B_E,
     &                       beta,
     &                       epsilon_E,
     &                       l_E,
     &                       rho_E_healthy,
     &                       k_E,
     &                       detJac,
     &                       detJaccur,
     &                       defgrad,
     &                       det_defgrad,
     &                       resEn,
     &                       tangEnU,
     &                       tangEnD,
     &                       tangEnEn,
     &                       fE1,
     &                       fE2)

        ! drug thread -----------------------------------------------------------------------------------------------------!
        call elmt50_lumen_drug(dt,
     &                         ttim,
     &                         Xn,
     &                         Un,
     &                         xi,
     &                         eta,
     &                         zeta,
     &                         rho_E_iter_e,
     &                         k_E,
     &                         c_d_iter_e,
     &                         c_d_wall_iter_e,
     &                         q_D_bar_ref,
     &                         A_D,
     &                         t_p,
     &                         t_c,
     &                         epsilon_D2,
     &                         Pdrug,
     &                         resD1,
     &                         tangD1U,
     &                         tangD1D,
     &                         tangD1En,
     &                         resD2,
     &                         tangD2U,
     &                         tangD2D,
     &                         tangD2En,
     &                         f_B2)


            ! Copy plot variables into the stress array for visualization -------------------------------------------------!
            ! (all the quantities in the current configuration)
            xsj(qp) = detJaccur
            shp(1:4,qp) = sf
            sig(12,qp) = k_E*rho_E_iter_qp/det_defgrad

            ! Time-history data of EC field for plotting ------------------------------------------------------------------!
            tt(12*qp) = sig(12,qp)

            if(isw.eq.3 .or. isw.eq.6) then

               ! Assign residual vector values ----------------------------------------------------------------------------!  
               res_p_qp(qp,:) = resP*detJac*weight
               res_t_qp(qp,:) = resT*detJac*weight
               res_en_qp(qp,:) = resEn*detJac*weight
               
               if (flg_sapp.EQ.0) then
                res_d_qp(qp,:) = resD1*detJac*weight
               else 
                res_d_qp(qp,:) = resD2*detJac*weight 
               end if

               if(isw.eq.3) then
                     ! Assign element matrix values -----------------------------------------------------------------------!  
                     kpu_qp(qp,:,:) = tangPU*detJac*weight
                     kpen_qp(qp,:,:) = tangPEn*detJac*weight

                     ktu_qp(qp,:,:) = tangTU*detJac*weight
                     kten_qp(qp,:,:) = tangTEn*detJac*weight

                     kenu_qp(qp,:,:) = tangEnU*detJac*weight
                     kend_qp(qp,:,:) = tangEnD*detJac*weight
                     kenen_qp(qp,:,:) = tangEnEn*detJac*weight

                     if (flg_sapp.EQ.0) then
                      kdu_qp(qp,:,:) = tangD1U*detJac*weight
                      kdd_qp(qp,:,:) = tangD1D*detJac*weight
                      kden_qp(qp,:,:) = tangD1En*detJac*weight
                     else
                      kdu_qp(qp,:,:) = tangD2U*detJac*weight
                      kdd_qp(qp,:,:) = tangD2D*detJac*weight
                      kden_qp(qp,:,:) = tangD2En*detJac*weight
                     end if
               end if
          end if
         end do

        else         ! parallel computation

        ! Set open MP threads = total number of quadrature points -----------------------------------------------------------!       
        call omp_set_num_threads(nqp)

        ! Enable nested parallelism -----------------------------------------------------------------------------------------!
        call omp_set_max_active_levels(np_levels)           

! Initialize quadrature point openMP fork -----------------------------------------------------------------------------------!       
!$omp parallel private(qp,xi,eta,zeta,weight,       
!$   * sf,rho_E_iter_qp,rho_E_prev_qp,c_d_wall_iter_qp,
!$   * tawss_iter_qp,osi_iter_qp,si_E,            
!$   * detJac,detJaccur,defgrad,det_defgrad, 
!$   * lfP,lfT,fE1,fE2,f_B2,
!$   * resP,resT,resEn,resD1,resD2, 
!$   * tangPU,tangPEn,tangTU,tangTEn, 
!$   * tangEnU,tangEnD,tangEnEn,
!$   * tangD1U,tangD1D,tangD1En,
!$   * tangD2U,tangD2D,tangD2En) 
!$   * shared(f_B3_temp,max_lf_d,A_D, 
!$   * sig,tt,xsj,shp,res_p_qp,res_t_qp,
!$   * res_d_qp,res_en_qp,
!$   * kpu_qp,kpen_qp,ktu_qp,kten_qp,
!$   * kenu_qp,kend_qp,kenen_qp,
!$   * kdu_qp,kdd_qp,kden_qp)

        qp = omp_get_thread_num()+1  ! Quadrature point identifier = thread rank + 1 (since ranks start from 0)

        ! Isoparametric coordinates and weights for quadrature points -------------------------------------------------------!
        xi = quadr(1,qp)
        eta = quadr(2,qp)
        zeta = 0.d00
        weight = quadr(3,qp)   
        
        ! Evaluate shape function at the quadrature point -------------------------------------------------------------------! 
        call shape_func_eval2D(xi, eta, sf)

        ! DOF values
        rho_E_iter_qp = dot_product(sf,rho_E_iter_e)
        rho_E_prev_qp = dot_product(sf,rho_E_prev_e)
        c_d_wall_iter_qp = dot_product(sf,c_d_wall_iter_e)
        tawss_iter_qp = dot_product(sf,tawss_iter_e)
        osi_iter_qp = dot_product(sf,osi_iter_e)
        
        ! Quadrature point residuals and tangent contributions --------------------------------------------------------------!
! Initialize DOF openMP fork ------------------------------------------------------------------------------------------------!
!$omp parallel               
!$omp sections
!$omp section
        ! PDGF+TGF-beta thread ----------------------------------------------------------------------------------------------!
        !write(*,*) 'PDGF+TGF DOFs, thread = ',omp_get_thread_num()
        !write(*,*) 'level = ',omp_get_level()        
        ! EC-shape index calculation
        si_E = max(0.05d00,
     &   exp(-s_E*((1.d00 - osi_iter_qp)**4)*tawss_iter_qp))


        call elmt50_lumen_pdgf_tgf(dt,
     &                             ttim,
     &                             Xn,
     &                             Un,
     &                             xi,
     &                             eta,
     &                             zeta,
     &                             rho_E_iter_e,
     &                             k_E,
     &                             rho_E_healthy,
     &                             si_E,
     &                             q_P_bar_ref,
     &                             l_B_P,
     &                             q_T_bar_ref,
     &                             l_B_T,
     &                             lfP,
     &                             lfT,
     &                             resP,
     &                             tangPU,
     &                             tangPEn,
     &                             resT,
     &                             tangTU,
     &                             tangTEn)                         

!$omp section
        ! EC thread ---------------------------------------------------------------------------------------------------------!
        !write(*,*) 'EC DOF, thread = ',omp_get_thread_num()
        !write(*,*) 'level = ',omp_get_level() 
        call elmt50_lumen_EC(dt,
     &                       Xn,
     &                       Un,
     &                       xi,
     &                       eta,
     &                       zeta,
     &                       c_d_iter_e,
     &                       k_D,
     &                       rho_E_iter_e,
     &                       rho_E_prev_e,
     &                       d_E,
     &                       eta_E,
     &                       A_E,
     &                       B_E,
     &                       beta,
     &                       epsilon_E,
     &                       l_E,
     &                       rho_E_healthy,
     &                       k_E,
     &                       detJac,
     &                       detJaccur,
     &                       defgrad,
     &                       det_defgrad,
     &                       resEn,
     &                       tangEnU,
     &                       tangEnD,
     &                       tangEnEn,
     &                       fE1,
     &                       fE2)

!$omp section
        ! drug thread -----------------------------------------------------------------------------------------------------!
        !write(*,*) 'drug DOF, thread = ',omp_get_thread_num()
        !write(*,*) 'level = ',omp_get_level(),'qp = ',qp 
        call elmt50_lumen_drug(dt,
     &                         ttim,
     &                         Xn,
     &                         Un,
     &                         xi,
     &                         eta,
     &                         zeta,
     &                         rho_E_iter_e,
     &                         k_E,
     &                         c_d_iter_e,
     &                         c_d_wall_iter_e,
     &                         q_D_bar_ref,
     &                         A_D,
     &                         t_p,
     &                         t_c,
     &                         epsilon_D2,
     &                         Pdrug,
     &                         resD1,
     &                         tangD1U,
     &                         tangD1D,
     &                         tangD1En,
     &                         resD2,
     &                         tangD2U,
     &                         tangD2D,
     &                         tangD2En,
     &                         f_B2)

!$omp end sections
!$omp end parallel     
! Merge DOF openMP fork ---------------------------------------------------------------------------------------------------! 

            ! Copy plot variables into the stress array for visualization -------------------------------------------------!
            ! (all the quantities in the current configuration)
            xsj(qp) = detJaccur
            shp(1:4,qp) = sf
            sig(12,qp) = k_E*rho_E_iter_qp/det_defgrad

            ! Time-history data of EC field for plotting ------------------------------------------------------------------!
            tt(12*qp) = sig(12,qp)

            if(isw.eq.3 .or. isw.eq.6) then

               ! Assign residual vector values ----------------------------------------------------------------------------!  
               res_p_qp(qp,:) = resP*detJac*weight
               res_t_qp(qp,:) = resT*detJac*weight
               res_en_qp(qp,:) = resEn*detJac*weight
               
               if (flg_sapp.EQ.0) then
                res_d_qp(qp,:) = resD1*detJac*weight
               else 
                res_d_qp(qp,:) = resD2*detJac*weight 
               end if

               if(isw.eq.3) then
                     ! Assign element matrix values -----------------------------------------------------------------------!  
                     kpu_qp(qp,:,:) = tangPU*detJac*weight
                     kpen_qp(qp,:,:) = tangPEn*detJac*weight

                     ktu_qp(qp,:,:) = tangTU*detJac*weight
                     kten_qp(qp,:,:) = tangTEn*detJac*weight

                     kenu_qp(qp,:,:) = tangEnU*detJac*weight
                     kend_qp(qp,:,:) = tangEnD*detJac*weight
                     kenen_qp(qp,:,:) = tangEnEn*detJac*weight

                     if (flg_sapp.EQ.0) then
                      kdu_qp(qp,:,:) = tangD1U*detJac*weight
                      kdd_qp(qp,:,:) = tangD1D*detJac*weight
                      kden_qp(qp,:,:) = tangD1En*detJac*weight
                     else
                      kdu_qp(qp,:,:) = tangD2U*detJac*weight
                      kdd_qp(qp,:,:) = tangD2D*detJac*weight
                      kden_qp(qp,:,:) = tangD2En*detJac*weight
                     end if
               end if
          end if 

!$omp end parallel
! Merge quadrature point openMP fork --------------------------------------------------------------------------------------!

      end if

      end if
      
      if ((isw.EQ.3).OR.(isw.EQ.6)) then

        do i = 1, nqp
         ! Assemble residual vectors --------------------------------------------------------------------------------------!  
         rp = rp + res_p_qp(i,:)
         rt = rt + res_t_qp(i,:)
         rd = rd + res_d_qp(i,:)
         ren = ren + res_en_qp(i,:)
     
         if (isw.EQ.3) then        
            ! Assemble element matrices -----------------------------------------------------------------------------------!  
            kpu = kpu + kpu_qp(i,:,:)
            kpen = kpen + kpen_qp(i,:,:)

            ktu = ktu + ktu_qp(i,:,:)
            kten = kten + kten_qp(i,:,:)

            kenu = kenu + kenu_qp(i,:,:)
            kend = kend + kend_qp(i,:,:)
            kenen = kenen + kenen_qp(i,:,:)

            kdu = kdu + kdu_qp(i,:,:) 
            kdd = kdd + kdd_qp(i,:,:) 
            kden = kden + kden_qp(i,:,:) 
         end if

        end do

      end if           
        
      if(isw.eq.3 .or. isw.eq.6) then

          r = 0.d00

          r(25:28) = rp
          r(33:36) = rt
          r(65:68) = -1.d00*ren
          r(49:52) = rd

          call FeapCoupledSortVec3D(r)

          !r = -1.d00*r
          
          if(isw.eq.3) then 

            s = 0.d00
            
            s(25:28,1:12) = kpu
            s(25:28,65:68) = kpen

            s(33:36,1:12) = ktu
            s(33:36,65:68) = kten

            s(65:68,1:12) = kenu
            s(65:68,49:52) = kend
            s(65:68,65:68) = kenen

            s(49:52,1:12) = kdu
            s(49:52,49:52) = kdd
            s(49:52,65:68) = kden

            call FeapCoupledSortMat3D(s)

          end if
      end if

      if ((isw.EQ.4).OR.(isw.EQ.8)) then

         lint = 4
         nnl = 4
  
       call slcn3dsurf_mod(sig,
     &                 shp,
     &                 xsj,
     &                 quadr,
     &                 lint,
     &                 nnl,
     &                 r,
     &                 s)
  
        end if

      end subroutine elmt50
