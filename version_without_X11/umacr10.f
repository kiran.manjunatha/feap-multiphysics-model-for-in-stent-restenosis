!$Id:$

      !Makro read in concentration input file

      subroutine umacr10(lct,ctl)

            !      * * F E A P * * A Finite Element Analysis Program

            !....  Copyright (c) 1984-2021: Regents of the University of California
            !                               All rights reserved

            !-----[--.----+----.----+----.-----------------------------------------]
            !     Modification log                                Date (dd/mm/year)
            !       Original version                                    01/11/2006
            !       1. Remove 'prt' from argument list                  09/07/2009
            !       2. Add 'uhelpfl' comment option                     19/10/2017
            !       3. Add 'help' comment option - replaces 'uhelpfl'   07/01/2019
            !-----[--.----+----.----+----.-----------------------------------------]
            !      Purpose:  User interface for adding solution command language
            !                instructions.

            !      Inputs:
            !         lct       - Command character parameters
            !         ctl(3)    - Command numerical parameters

            !      Outputs:
            !         N.B.  Users are responsible for command actions.  See
            !               programmers manual for example.
            !-----[--.----+----.----+----.-----------------------------------------]
            implicit none

            include  'iofile.h'
            include  'umac1.h'
            include  'sdata.h'  ! ndf, ndm, nst, n_el
            include  'cdata.h'  ! numel
            include  'eldata.h'
            include  'pointer.h'! np(*) pointers
            include  'comblk.h' ! mr(*) and hr(*) arrays
            include  'cdat1.h'  ! ndd, nie
            include  'region.h'
            include  'hdata.h'  ! history pointers
            include  'tdata.h'

            logical       :: palloc, setvar
            integer       :: iact, k1,k2,k3,nnn
            real (kind=8) :: ct(3)

            character (len=15) :: lct

            logical       :: pcomp
            real (kind=8) :: ctl(3)

            real(kind=16), allocatable :: concentrations(:)
            integer, allocatable :: node_number(:)
            character(len=500) :: filename

            integer :: i, col_num
            character(len=500) :: line
            integer :: unit, num_lines
            integer :: num_values
            integer :: z
            real(kind=16), dimension(:), allocatable :: values
            integer :: iostatus
            logical :: start = .FALSE.

            save

            !     Set command word
            if (pcomp(uct,'ma10',4)) then      ! Usual    form
            uct = 'xnsm'                       ! Specify 'name'
            elseif (pcomp(lct,'help',4)) then  ! Write help information
            write(*,*) 'COMMAND: Name and description'
            elseif (urest.eq.1) then           ! Read  restart data
            elseif (urest.eq.2) then           ! Write restart data
            else                               ! Perform user operation
                  start = .TRUE.
            endif

            if(start == .TRUE.) then
            !Parse nodes_conc.txt from csv to concentration array
            filename = "nodes_conc.txt"

            print *, "Parse concentrations from"
            WRITE(*,*) filename

            col_num = 5
            ! Open the file for reading
            open(unit=unit, file=filename, action='read')
            num_lines = 0
            do
                  read(unit, '(A)', iostat=iostatus) line
                  if (iostatus < 0) exit
                        num_lines = num_lines + 1
            end do

            ! Allocate the concentrations array
            allocate(concentrations(num_lines))
            allocate(node_number(num_lines))

            ! Rewind the file to the beginning
            rewind(unit)

            ! Read and store the value at col_num in each line
            do i = 1, num_lines
                  read(unit, '(A)', iostat=iostatus) line
                  if (iostatus < 0) exit  ! Exit loop if end of file is reached

                  num_values = CountOccurrences(line, ',')
                  allocate(values(num_values + 1))
                  read(line, *, iostat=iostatus) values

                  ! Check if the line has enough values
                  if (size(values) < col_num) then
                        deallocate(values)
                        exit  
                  endif

                  ! Assign the value at col_num to concentrations
                  concentrations(i) = values(5)
                  node_number(i) = values(1)
                  
                  
                  WRITE(*, '(ES30.20)') values(1)

                  deallocate(values)
            end do

            ! Close the file
            close(unit)


            !save concentration array in solution array
            do z = 1, num_lines
            hr(np(40) + ndf * node_number(z) - 3) = concentrations(z) 
            end do


            deallocate(concentrations)
            deallocate(node_number)

            print *, num_lines
            print *, "Success, finished CSV parsing and stored in conc"

            if (ttim.gt.0.d00) then
                  print *, "calling pnewpr"
                  nreg = -1
                  call pnewpr()
            endif
            endif


            contains

            function CountOccurrences(str, target) result(count)
            implicit none
            character(len=*), intent(in) :: str
            character(len=1), intent(in) :: target
            integer :: count, i

            count = 0
            do i = 1, len(str)
                  if (str(i:i) == target) then
                        count = count + 1
                  endif
            end do
            end function CountOccurrences


      end subroutine umacr10




